/*
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WYRD_PROPERTIES_H
#define WYRD_PROPERTIES_H

#include <wyrd.h>

#include <wyrd/handle.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/*****************************************************************************
 * Low-level API
 */

/**
 * Properties have a name, a type, and a value. They also have a merge strategy.
 *
 * The name is an UTF-8 string. This may change in future to better support
 * non-western scripts, but for the time being, this is the simplest choice.
 *
 * The value and type are linked. Supported property types are related to C
 * primitive types (integers, UTF-8 strings). Additionally, a value may be
 * a binary large object (BLOB) or a container. Containers hold other properties,
 * but no value of their own. Support for more property types is planned, but
 * currently unavailable.
 */
typedef enum {
  WYRD_PT_UNKNOWN         = 0x00, /**< Unknown property type */

  WYRD_PT_BLOB            = 0x01, /**< The property is a binary large object
                                       (BLOB), i.e. we don't know anything about
                                       it except its size. */
  WYRD_PT_UTF8_STRING     = 0x02, /**< The property is a UTF-8 encoded string. */

  WYRD_PT_UINT8           = 0x0a, /**< uint8_t */
  WYRD_PT_SINT8           = 0x0b, /**< int8_t */
  WYRD_PT_UINT16          = 0x0c, /**< uint16_t */
  WYRD_PT_SINT16          = 0x0d, /**< int16_t */
  WYRD_PT_UINT32          = 0x0e, /**< uint32_t */
  WYRD_PT_SINT32          = 0x0f, /**< int32_t */
  WYRD_PT_UINT64          = 0x10, /**< uint64_t */
  WYRD_PT_SINT64          = 0x11, /**< int64_t */

  WYRD_PT_BOOL            = 0x20, /**< bool */

  WYRD_PT_FLOAT           = 0x30, /**< float */
  WYRD_PT_DOUBLE          = 0x31, /**< double */

  // A sequential list type; items can be duplicated
  WYRD_PT_CONT_LIST       = 0x40, /**< A list-like container. */

  // A set type; items are unique
  // WYRD_PT_CONT_SET        = 0x41,

  // (string) key to value mappings; items can be unique or duplicated
  WYRD_PT_CONT_MAP        = 0x42, /**< A key/value map; keys are always
                                       UTF-8 strings, values can be any property
                                       type. */
  // WYRD_PT_CONT_MULTI_MAP  = 0x42,

  // TODO add more property types
} wyrd_property_type;


/**
 * Property merge strategies are essentially equivalent to differnt CRDT
 * implementations.
 *
 * It's *relatively* easy merging a distributed set. Operations either
 * add or remove an entry, etc. But merging other kinds of edits can be
 * far more complex, and depend on the data type.
 *
 * The behaviour of property is effectively specified by the combination
 * of a wyrd_property_type and a corresponding merge strategy. Note that
 * not all data types are compatible with all merge strategies.
 */
typedef enum {
  /**
   * The naive override strategy simply applies values in the order that
   * edits arrive. This is almost certainly *not* what you want, but it
   * is a good base for comparing other merge strategies' behaviour.
   */
  WYRD_MS_NAIVE_OVERRIDE = 0,

  // TODO add more merge strategies
} wyrd_merge_strategy;


/**
 * Set a property at a given path to a value.
 *
 * Paths can be flat names; however, if a dot character ('.', U+002E) is
 * encountered, it is considered a path separator. It is thus possible to
 * create structured data by e.g. setting the values of e.g. "foo.bar" and
 * "foo.baz" separately.
 *
 * Note that empty path segments are ignored, i.e. ".foo.bar" is equivalent to
 * "foo.bar", and "foo..baz" to "foo.baz", etc.
 *
 * When override_type is zero, setting a property value will fail if the
 * property previously had a value, and the type/strategy are other than the
 * provided. This may e.g. trigger if a property "foo" exists, and a property
 * "foo.bar" is to be set, as this would change the type of "foo" to a
 * container. If non-zero values are given, value types are silently
 * overridden.
 *
 * This function is the most primitive form of the API. Prefer using the
 * simpler versions below.
 *
 * @param [in] handle A handle to an opened resource.
 * @param [in] path Path that names the property in question.
 * @param [in] type The property type; see \ref wyrd_property_type.
 * @param [in] strategy The merge strategy to use; see \ref
 *    wyrd_merge_strategy.
 * @param [in] value A pointer to the value; how this is interpreted depends
 *    on \p type.
 * @param [in] value_size The size of the value; if this does not match
 *    expecttions provided by \p type, the function may error.
 * @param [in] override_type A boolean value defining whether an existing
 *    property value's type should be overridden by this call (TRUE), or
 *    if the function should error when the existing type does not match
 *    the requested type (FALSE).
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_VALUE If any of the inpurt values are invalid,
 *    such as NULL pointers where there should not be any, etc.
 * @retval WYRD_ERR_BAD_PROPERTY_TYPE If \p override_type is set to FALSE
 *    and the document conditions occur.
 */
WYRD_API wyrd_error_t
wyrd_set_property_typed(struct wyrd_handle * handle, char const * path,
    wyrd_property_type type, wyrd_merge_strategy strategy,
    void const * value, size_t value_size,
    int override_type);


/**
 * Equivalent getter to \ref wyrd_set_property_typed() above.
 *
 * Pass the maximum value size in the last parameter, i.e. the size of the value
 * buffer. On success, value_size is set to the actual amount of buffer used. On
 * WYRD_ERR_OUT_OF_MEMORY, value_size is set to the amount of buffer needed to
 * hold the full value.
 *
 * A type is *not* provided. You can query the type with get_property_type()
 * below.
 *
 * Note that this function will always return the *current* value for a
 * property. If you wish to examine change sets, see edits.h instead.
 *
 * @param [in] handle A handle to an opened resource.
 * @param [in] path Path that names the property in question.
 * @param [out] value A pointer to the value; how this is interpreted depends
 *    on \p type.
 * @param [in, out] value_size When calling, the size of the output buffer
 *    provided in \p value. On success, the amount of output buffer consumed.
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_VALUE If any of the inpurt values are invalid,
 *    such as NULL pointers where there should not be any, etc.
 * @retval WYRD_ERR_BAD_PROPERTY_TYPE When a container property is requested;
 *    we do not yet have a generic mechanism for doing so.
 * @retval WYRD_ERR_OUT_OF_MEMORY If the output buffer size given by
 *    \p value_size is too small for the property value.
 */
WYRD_API wyrd_error_t
wyrd_get_property(struct wyrd_handle * handle, char const * path,
    void * value, size_t * value_size);


/**
 * Retrieve the type of a property.
 *
 * If the property is not found, an error is returned. On WYRD_ERR_SUCCESS,
 * type will give the type of the named property.
 *
 * @param [in] handle A handle to an opened resource.
 * @param [in] path The path to the property.
 * @param [out] type The curent property type.
 *
 * @retval WYRD_ERR_SUCCESS If the given property type matches the current
 *    property type.
 * @retval WYRD_ERR_INVALID_VALUE If any of the parameters are invalid, such as
 *    e.g. NULL when they should not be, etc.
 * @retval WYRD_ERR_PROPERTY_NOT_FOUND If the specified property does not
 *    exist.
 */
WYRD_API wyrd_error_t
wyrd_get_property_type(struct wyrd_handle * handle, char const * path,
    wyrd_property_type * type);


/**
 * Check if the given property type currently applies to the given path.
 *
 * This is equivalent to invoking \ref wyrd_get_property_type, and comparing
 * the result to your expected property type.
 *
 * @param [in] handle A handle to an opened resource.
 * @param [in] path The path to the property.
 * @param [in] type The expected property type.
 *
 * @retval WYRD_ERR_SUCCESS If the given property type matches the current
 *    property type.
 * @retval WYRD_ERR_INVALID_VALUE If any of the parameters are invalid, such as
 *    e.g. NULL when they should not be, etc.
 * @retval WYRD_ERR_BAD_PROPERTY_TYPE If the property types do NOT match.
 * @retval WYRD_ERR_PROPERTY_NOT_FOUND If the specified property does not
 *    exist.
 */
WYRD_API wyrd_error_t
wyrd_check_property_type(struct wyrd_handle * handle, char const * path,
    wyrd_property_type type);


/**
 * Retrieve the merge strategy of a property.
 *
 * @param [in] handle A handle to an opened resource.
 * @param [in] path The path to the property.
 * @param [out] strategy The curent merge strategy.
 *
 * @retval WYRD_ERR_SUCCESS If the given merge strategy matches the current
 *    merge strategy.
 * @retval WYRD_ERR_INVALID_VALUE If any of the parameters are invalid, such as
 *    e.g. NULL when they should not be, etc.
 * @retval WYRD_ERR_PROPERTY_NOT_FOUND If the specified property does not
 *    exist.
 */
WYRD_API wyrd_error_t
wyrd_get_merge_strategy(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy * strategy);

/**
 * Check if the given merge strategy currently applies to the given path.
 *
 * This is equivalent to invoking \ref wyrd_get_merge_strategy, and comparing
 * the result to your expected merge strategy.
 *
 * @param [in] handle A handle to an opened resource.
 * @param [in] path The path to the property.
 * @param [in] strategy The expected merge strategy.
 *
 * @retval WYRD_ERR_SUCCESS If the given merge strategy matches the current
 *    merge strategy.
 * @retval WYRD_ERR_INVALID_VALUE If any of the parameters are invalid, such as
 *    e.g. NULL when they should not be, etc.
 * @retval WYRD_ERR_BAD_PROPERTY_TYPE If the merge strategies do NOT match.
 * @retval WYRD_ERR_PROPERTY_NOT_FOUND If the specified property does not
 *    exist.
 */
WYRD_API wyrd_error_t
wyrd_check_merge_strategy(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy);


/**
 * The property callback type; this is called (when a callback is registered),
 * and a named property has been modified. The values passed are the newly set
 * values.
 *
 * @param [in] handle A handle to an opened resource.
 * @param [in] path The path to the property.
 * @param [in] type The newly set property type.
 * @param [in] value The newly set property value.
 * @param [in] value_size Size in octets of \p value.
 * @param [in] baton An opaque pointer registered with the callback.
 */
typedef void (* wyrd_property_callback)(struct wyrd_handle * handle,
    char const * path, wyrd_property_type type,
    void const * value, size_t value_size, void * baton);

/**
 * Set observation callbacks for properties.
 *
 * Callbacks get invoked when the property changes. The baton you provide when
 * setting a callback is passed to the callback as the final parameter. Note
 * that these functions do not take ownership of any data; the caller must take
 * care to scope lifetime appropriately.
 *
 * Generally speaking, the tuple of path, callback and baton is treated as
 * unique. However, due to the way these callbacks may be integrated to hook
 * into other event/signalling frameworks, the add(set) operation increments
 * an internal reference counter on that tuple. Likewise, the remove operation
 * decrements the reference count. By contrast, the clear operation removes the
 * tuple completely.
 *
 * This effectively yields two modes of operation: using add/remove you make
 * use of the reference counting. Using add/clear, operations effectively act
 * as idempotent.
 *
 * As a final note, callbacks *may* be invoked on threads other than the main
 * thread. They should terminate relatively quickly as not to block background
 * threads, and access data in a thread-safe manner.
 *
 * @param [in] handle A handle to an opened resource.
 * @param [in] path The path to the property to observe.
 * @param [in] callback A \ref wyrd_property_callback provided by the user.
 * @param [in] baton An opaque pointer that will be handled to \p callback
 *    if it gets invoked.
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_VALUE If any of the parameters are invalid, such as
 *    e.g. NULL when they should not be, etc.
 */
WYRD_API wyrd_error_t
wyrd_add_property_callback(struct wyrd_handle * handle, char const * path,
    wyrd_property_callback callback, void * baton);

/**
 * Alias for \ref wyrd_add_property_callback.
 */
#define wyrd_set_property_callback wyrd_add_property_callback

/**
 * Remove one *instance* of the given callback/baton combination from observing
 * the property at the given path.
 *
 * To ease development for users, calls to \ref wyrd_add_property_callback
 * for each combination are reference counted. This function removes one such
 * reference, and removes the combination entirely if the reference count drops
 * to zero.
 *
 * @param [in] handle A handle to an opened resource.
 * @param [in] path The path to the property to observe.
 * @param [in] callback A \ref wyrd_property_callback provided by the user.
 * @param [in] baton An opaque pointer that will be handled to \p callback
 *    if it gets invoked.
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_VALUE If any of the parameters are invalid, such as
 *    e.g. NULL when they should not be, etc.
 */
WYRD_API wyrd_error_t
wyrd_remove_property_callback(struct wyrd_handle * handle, char const * path,
    wyrd_property_callback callback, void * baton);

/**
 * Immediately remove the callback/baton combination from observing the
 * property at the given path.
 *
 * @param [in] handle A handle to an opened resource.
 * @param [in] path The path to the property to observe.
 * @param [in] callback A \ref wyrd_property_callback provided by the user.
 * @param [in] baton An opaque pointer that will be handled to \p callback
 *    if it gets invoked.
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_VALUE If any of the parameters are invalid, such as
 *    e.g. NULL when they should not be, etc.
 */
WYRD_API wyrd_error_t
wyrd_clear_property_callback(struct wyrd_handle * handle, char const * path,
    wyrd_property_callback callback, void * baton);



/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_BLOB.
 */
WYRD_API wyrd_error_t
wyrd_set_property_blob(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    void const * value, size_t value_size, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_BLOB first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_blob(struct wyrd_handle * handle, char const * path,
    void * value, size_t * value_size);


/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_UTF8_STRING.
 */
WYRD_API wyrd_error_t
wyrd_set_property_utf8string(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    char const * value, size_t value_size, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_UTF8_STRING first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_utf8string(struct wyrd_handle * handle, char const * path,
    char * value, size_t * value_size);

/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_UINT8.
 */
WYRD_API wyrd_error_t
wyrd_set_property_uint8(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    uint8_t value, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_UINT8 first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_uint8(struct wyrd_handle * handle, char const * path,
    uint8_t * value);

/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_SINT8.
 */
WYRD_API wyrd_error_t
wyrd_set_property_sint8(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    int8_t value, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_SINT8 first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_sint8(struct wyrd_handle * handle, char const * path,
    int8_t * value);

/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_UINT16.
 */
WYRD_API wyrd_error_t
wyrd_set_property_uint16(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    uint16_t value, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_UINT16 first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_uint16(struct wyrd_handle * handle, char const * path,
    uint16_t * value);

/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_SINT16.
 */
WYRD_API wyrd_error_t
wyrd_set_property_sint16(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    int16_t value, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_SINT16 first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_sint16(struct wyrd_handle * handle, char const * path,
    int16_t * value);

/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_UINT32.
 */
WYRD_API wyrd_error_t
wyrd_set_property_uint32(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    uint32_t value, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_UINT32 first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_uint32(struct wyrd_handle * handle, char const * path,
    uint32_t * value);

/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_SINT32.
 */
WYRD_API wyrd_error_t
wyrd_set_property_sint32(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    int32_t value, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_SINT32 first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_sint32(struct wyrd_handle * handle, char const * path,
    int32_t * value);

/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_SINT64.
 */
WYRD_API wyrd_error_t
wyrd_set_property_sint64(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    int64_t value, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_SINT64 first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_sint64(struct wyrd_handle * handle, char const * path,
    int64_t * value);

/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_UINT64.
 */
WYRD_API wyrd_error_t
wyrd_set_property_uint64(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    uint64_t value, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_UINT64 first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_uint64(struct wyrd_handle * handle, char const * path,
    uint64_t * value);

/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_FLOAT.
 */
WYRD_API wyrd_error_t
wyrd_set_property_float(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    float value, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_FLOAT first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_float(struct wyrd_handle * handle, char const * path,
    float * value);

/**
 * See \ref wyrd_set_property_typed(), but sets the type parameter to
 * \ref WYRD_PT_DOUBLE.
 */
WYRD_API wyrd_error_t
wyrd_set_property_double(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    double value, int override_type);

/**
 * See \ref wyrd_get_property(), but checks if the property value is
 * \ref WYRD_PT_DOUBLE first.
 */
WYRD_API wyrd_error_t
wyrd_get_property_double(struct wyrd_handle * handle, char const * path,
    double * value);


#if 0

/**
 * Property iterators iterate over all properties in a given resource.
 */
struct wyrd_property_iter;

/**
 * Create an property iterator from a handle. You can specify whether to
 * iterate depth-first (depth_first != 0) or breadth-first (depth_first == 0).
 */
WYRD_API wyrd_error_t
wyrd_property_iter_create(struct wyrd_handle * handle,
    struct wyrd_property_iter ** iter, int depth_first);

WYRD_API wyrd_error_t
wyrd_property_iter_free(struct wyrd_property_iter ** iter);


/**
 * Increment an property iterator.
 *
 * Iterating past the end produces an invalid iterator. You can still call
 * this next function on an invalid iterator, but it will not do anything.
 */
WYRD_API wyrd_error_t
wyrd_property_iter_next(struct wyrd_property_iter * iter);

/**
 * Test an iterator for validity; see the next function above.
 */
WYRD_API wyrd_error_t
wyrd_property_iter_valid(struct wyrd_property_iter const * iter);


/**
 * Retrieve iterator path, or NULL. Ownership remains with the
 * iterator/document.
 */
WYRD_API char const *
wyrd_property_iter_path(struct wyrd_property_iter const * iter,
    size_t * path_size);

#endif


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
