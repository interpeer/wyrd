/*
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_ERROR_H
#define WYRD_ERROR_H

#include <wyrd.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * Macros for building a table of known error codes and associated messages.
 **/
#if !defined(WYRD_START_ERRORS)
#define WYRD_START_ERRORS \
  typedef uint32_t wyrd_error_t; \
  enum wyrd_error_code {
#define WYRD_ERRDEF(name, code, desc) WYRD_ ## name = code,
#define WYRD_END_ERRORS \
    WYRD_ERROR_LAST, \
    /**< Error codes larger than or equal to this value are not defined. */ \
    WYRD_START_USER_RANGE = 1000 \
    /**< Any user-defined errors should be added to this offset. */ \
  };
#define WYRD_ERROR_FUNCTIONS
#endif


/*****************************************************************************
 * Error definitions
 **/

/**
 * @typedef wyrd_error_t
 * The error code type for the caprock library.
 */

/**
 * @enum wyrd_error_code
 * All valid values for @ref wyrd_error_t are defined in this enumeration.
 */


WYRD_START_ERRORS

WYRD_ERRDEF(ERR_SUCCESS,
    0,
    "No error")
    /**< No error */

WYRD_ERRDEF(ERR_UNEXPECTED,
    1,
    "Nobody expects the Spanish Inquisition!")
    /**< An unexpected error occurred. This should only be returned from code
     * branches that should never be reached, and indicates a bug in the
     * library. */


WYRD_ERRDEF(ERR_NOT_IMPLEMENTED,
    2,
    "The requested functionality is not yet implemented.")
    /**< This functionality is not yet implemented. */

WYRD_ERRDEF(ERR_INVALID_VALUE,
    3,
    "An invalid value was provided as a parameter.")
    /**< Similar to POSIX EINVAL, an invalid parameter was provided. */

WYRD_ERRDEF(ERR_INITIALIZATION,
    4,
    "Failed to initialize a resource.")
    /**< Failed to initialize a resource. */

WYRD_ERRDEF(ERR_OUT_OF_MEMORY,
    5,
    "Out of memory.")
    /**< Similar to POSIX ENOMEM, we reached a condition where there was
         insufficient memory available. */

WYRD_ERRDEF(ERR_ACCESS_DENIED,
    10,
    "Access to a file/resource was denied.")
    /**< Access to a file/resource was denied. */

WYRD_ERRDEF(ERR_FS_ERROR,
    11,
    "A file/resource or one of its parent path components does not exist, or "
    "could not be resolved.")
    /**< A file/resource or one of its parent path components does not exist,
         or could not be resolved. */

WYRD_ERRDEF(ERR_IO_ERROR,
    12,
    "Reading from/writing to a file or resource failed.")
    /**< Reading from/writing to a file or resource failed. */

WYRD_ERRDEF(ERR_PROPERTY_NOT_FOUND,
    20,
    "Property not found in document.")
    /**< Property not found in document. */

WYRD_ERRDEF(ERR_EDIT_NOT_FOUND,
    21,
    "No matching edit found in document.")
    /**< No matching edit found in document. */

WYRD_ERRDEF(ERR_BAD_PROPERTY_TYPE,
    22,
    "Tried accessing a property as the wrong type.")
    /**< Tried accessing a property as the wrong type. */

WYRD_ERRDEF(ERR_BAD_MERGE,
    23,
    "Could not merge property values.")
    /**< Could not merge property values. */

WYRD_ERRDEF(ERR_BAD_MERGE_STRATEGY,
    24,
    "The given merge strategy does not match the one on record.")
    /**< The given merge strategy does not match the one on record. */

WYRD_ERRDEF(ERR_END_OF_RESOURCE,
    24,
    "While reading, the end of the resource was reached.")

WYRD_ERRDEF(ERR_ITERATOR_INVALID,
    30,
    "Invalid iterator; this could mean it is at the end.")
    /**< Invalid iterator; this could mean it is at the end. */

WYRD_ERRDEF(ERR_SERIALIZATION,
    40,
    "Error occurred when serializing/deserializing.")
    /**< Serialization or deserialization failed. */


// TODO add errors as required

WYRD_END_ERRORS


#if defined(WYRD_ERROR_FUNCTIONS)

/*****************************************************************************
 * Functions
 **/

/**
 * Return the error message associated with the given error code. Never returns
 * nullptr; if an unknown error code is given, an "unidentified error" string is
 * returned.
 *
 * @param [in] code The error code to query.
 * @return The message defined above to describe the error code.
 **/
WYRD_API char const * wyrd_error_message(wyrd_error_t code);

/**
 * Return a string representation of the given error code. Also never returns
 * nullptr, see \ref wyrd_error_message() above.
 *
 * @param [in] code The error code to query.
 * @return The name of the error constant (enum value) as a string.
 **/
WYRD_API char const * wyrd_error_name(wyrd_error_t code);


#endif // WYRD_ERROR_FUNCTIONS


/**
 * Undefine macros again
 **/
#undef WYRD_START_ERRORS
#undef WYRD_ERRDEF
#undef WYRD_END_ERRORS

#undef WYRD_ERROR_FUNCTIONS

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif // guard
