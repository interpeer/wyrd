/*
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WYRD_API_H
#define WYRD_API_H

#include <wyrd.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * \struct wyrd_api
 * Wyrd API handle; contains "global" data, for better compatibility with
 * shared object libraries.
 */
struct wyrd_api;

/**
 * Create an API instance. Other functions require this API instance to work,
 * so calling this function is the first thing you should do.
 *
 * Before calling, api should be initialized to NULL. After calling, api should
 * be non-NULL and WYRD_ERR_SUCCESS should be returned.
 *
 * If you call this function with an already initialized api instance, that
 * instance will be destroyed and a new instance created.
 *
 * @param [in, out] api The value will be set to an initialized API instance.
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_VALUE If any of the parameters are invalid, such as
 *    e.g. NULL when they should not be, etc.
 */
WYRD_API wyrd_error_t
wyrd_create(struct wyrd_api ** api);

/**
 * Destroy an API instance created with wyrd_create().
 *
 * @param [in, out] api The value will be set to NULL on success.
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_VALUE If any of the parameters are invalid, such as
 *    e.g. NULL when they should not be, etc.
 */
WYRD_API wyrd_error_t
wyrd_destroy(struct wyrd_api ** api);


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
