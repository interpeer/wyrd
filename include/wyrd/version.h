/*
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_VERSION_H
#define WYRD_VERSION_H

#include <wyrd.h>

#include <wyrd/error.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * The library's major, minor and patch version numbers are provided in this
 * struct.
 */
struct wyrd_version_data
{
  uint16_t major; /**< library major version */
  uint16_t minor; /**< library minor version */
  uint16_t patch; /**< library patch version */
};

/**
 * @return the library's version number.
 */
WYRD_API struct wyrd_version_data wyrd_version(void);

/**
 * @return a copyright string for this library; the returned value must not be
 * freed.
 */
WYRD_API char const * wyrd_copyright_string(void);

/**
 * Return a license string for this library; the returned value must not be
 * freed.
 */
WYRD_API char const * wyrd_license_string(void);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
