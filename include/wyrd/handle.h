/*
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WYRD_HANDLE_H
#define WYRD_HANDLE_H

#include <wyrd.h>

#include <wyrd/api.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * \struct wyrd_handle
 * Wyrd resources are represented by a handle, which needs to be opened (and
 * closed).
 */
struct wyrd_handle;

/**
 * Flags for opening a handle are ORed together. We don't define many here yet.
 */
typedef enum {
  /** No open mode; this is basically useless. */
  WYRD_O_NONE         = 0x00,
  /** Open in read mode *only* */
  WYRD_O_READ         = 0x01,
  /** Open in write mode *only* */
  WYRD_O_WRITE        = 0x02,
  /** Open in read and write mode */
  WYRD_O_RW           = WYRD_O_READ | WYRD_O_WRITE,
  /** Create file if it doesn't exist yet. */
  WYRD_O_CREATE       = 0x04,
  /** When encountering unknown parts to the file, try to recover and skip
      them. This may be useful for error recovery, but also means you may
      miss some important changes. */
  WYRD_O_SKIP_UNKNOWN = 0x08,
} wyrd_open_flags;


/**
 * Opening requires an API handle, a wyrd_handle to represent the resources,
 * and flags.
 *
 * We also need a file name for a local wyrd resource. For shared resources,
 * we need an identifier instead.
 *
 * @param [in] api An initialized API instance.
 * @param [out] handle Return a handle to the opened file on success.
 * @param [in] filename The name of the file to open.
 * @param [in] flags Open flags; see @ref wyrd_open_flags.
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_VALUE If any of the inpurt values are invalid,
 *    such as NULL pointers where there should not be any, etc.
 * @retval WYRD_ERR_UNEXPECTED If unspecific system errors occurred.
 * @retval WYRD_ERR_NOT_IMPLEMENTED If the function is not implemented on
 *    the current platform.
 * @retval Other If the passed handle was already opened, needed to be closed,
 *    and closing the handle did not return successfully.
 */
WYRD_API wyrd_error_t
wyrd_open_file(struct wyrd_api * api, struct wyrd_handle ** handle,
    char const * filename, int flags);

/**
 * Open a vessel resource with the given identifier.
 *
 * \rst
 *
 * .. note::
 *    The identifier currently specifies a file name for a vessel resource;
 *    in future, the resource identifier will be used.
 *
 * \endrst
 *
 * You can set additional resource options via the @ref wyrd_set_resource_option
 * function. In fact, when adding properties to a resource, you should at minimum
 * set an author and a set of resource algorithms.
 *
 * @param [in] api An initialized API instance.
 * @param [out] handle Return a handle to the opened resource on success.
 * @param [in] identifier The name of the resource to open.
 * @param [in] flags Open flags; see @ref wyrd_open_flags.
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_VALUE If any of the inpurt values are invalid,
 *    such as NULL pointers where there should not be any, etc.
 * @retval WYRD_ERR_UNEXPECTED If unspecific system errors occurred.
 * @retval WYRD_ERR_NOT_IMPLEMENTED If the function is not implemented on
 *    the current platform.
 * @retval Other If the passed handle was already opened, needed to be closed,
 *    and closing the handle did not return successfully.
 */
WYRD_API wyrd_error_t
wyrd_open_resource(struct wyrd_api * api, struct wyrd_handle ** handle,
    char const * identifier, int flags);

/**
 * The enumerator defines options that can be set on resources opened via
 * the @ref wyrd_open_resource() function. Use @ref wyrd_set_resource_option
 * to set options.
 */
typedef enum {
  WYRD_RO_AUTHOR      = 1,
  /**< This option specifies the author structure to use when creating
       resources.

       The value type is expected to be `struct vessel_author *`; the
       function will *not* copy the structure, nor take ownership, so
       you have to ensure it exists for at least as long as the handle.
  */
  WYRD_RO_ALGORITHMS  = 2,
  /**< Provide the algorithm options to use when creating resources.

       The value type is expected to be `struct vessel_algorithm_options *`;
       the value is copied by this function, so you can discard the parameter
       after calling.
   */
} wyrd_resource_options;


/**
 * Set resource options on an open handle representing a vessel resource.
 *
 * @param [in] handle The opened vessel resource handle.
 * @param [in] option The type of option to set.
 * @param [in] value A pointer to a option type specific value.
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_HANDLE If the handle was opened or the wrong type.
 *    This is also returned if the option was unknown.
 */
WYRD_API wyrd_error_t
wyrd_set_resource_option(struct wyrd_handle * handle,
    wyrd_resource_options option, void * value);

/**
 * Closing a resource just requires the handle.
 *
 * @param [in, out] handle The handle to close; it'll be set to NULL on success.
 *
 * @retval WYRD_ERR_SUCCESS On success.
 * @retval WYRD_ERR_INVALID_VALUE If any of the parameters are invalid, such as
 *    e.g. NULL when they should not be, etc.
 * @retval WYRD_ERR_NOT_IMPLEMENTED If called on handle whose resource type is not
 *    supported.
 * @retval Other When the specific resource type's handle code returns a
 *    specific failure.
 */
WYRD_API wyrd_error_t
wyrd_close(struct wyrd_handle ** handle);


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
