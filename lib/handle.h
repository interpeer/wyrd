/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_LIB_HANDLE_H
#define WYRD_LIB_HANDLE_H

#include <build-config.h>

#include <wyrd/handle.h>

#include <memory>
#include <string>

#include "api.h"
#include "properties.h"
#include "callbacks.h"

namespace wyrd {

enum handle_type
{
  HT_POSIX  = 0,
  HT_VESSEL = 1,
};

/**
 * The interface for handles is file-like, but also leans on how vessel needs
 * to know how much space to allocate for writing.
 *
 * Constructors open, and close() closes. For reading, read() works just like
 * files.
 *
 * For writing, we alloocate a buffer, which can be committed or discarded.
 */
struct handle
{
  // General
  std::shared_ptr<wyrd::api>  api;
  handle_type                 type;

  // The root property is a map.
  property                    properties{WYRD_PT_CONT_MAP};

  // Property callbacks
  property_callbacks          callbacks = {};

  // Resource access
  std::string                 identifier;
  int                         flags; // From open call

  // The root property is a map.
  property  _properties{WYRD_PT_CONT_MAP};

  struct range
  {
    inline range(size_t _size)
      : m_size{_size}
    {
    }

    virtual ~range() = default;

    inline size_t size() const
    {
      return m_size;
    }

    virtual char * buffer() = 0;
    virtual wyrd_error_t commit() = 0;
    virtual wyrd_error_t discard() = 0;

  private:
    size_t m_size;
  };


  inline handle(std::shared_ptr<wyrd::api> const & _api,
      handle_type _type,
      std::string const & _identifier,
      int _flags)
    : api{_api}
    , type{_type}
    , identifier{_identifier}
    , flags{_flags}
  {
  }

  virtual ~handle() = default;


  virtual wyrd_error_t close() = 0;

  virtual wyrd_error_t read(void * buf, size_t & count) = 0;

  virtual wyrd_error_t allocate(std::unique_ptr<range> & rng, size_t size) = 0;
};


/**
 * Function for persisting an edit into a handle.
 */
WYRD_PRIVATE wyrd_error_t
persist_edit(std::shared_ptr<handle> _handle, std::shared_ptr<edit> _edit);

} // namespace wyrd

#ifdef WYRD_POSIX
#include "handles/posix_handle.h"
#endif
#include "handles/vessel_handle.h"

// TODO include vessel handle when implemented

extern "C" {

struct wyrd_handle
{
  std::shared_ptr<wyrd::handle> handle;
};

} // extern "C"

#endif // guard
