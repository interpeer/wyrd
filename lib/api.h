/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_LIB_API_H
#define WYRD_LIB_API_H

#include <build-config.h>

#include <wyrd/api.h>

#include <memory>
#include <map>

#include <vessel/resource.h>

#include "error.h"
#include "merge_strategy.h"

namespace wyrd {


static char const * DEFAULT_VESSEL_ALGOS = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";

/**
 * The actual API instance
 */
struct api
{
  // Merge strategies - they should be instanciated only once per API instance.
  using strategy_map = std::map<
    wyrd_merge_strategy,
    std::shared_ptr<wyrd::merge_strategy>
  >;
  strategy_map                      merge_strategies = {};

  // Vessel contexts.
  // TODO in future, we may want to set this from the outside.
  struct vessel_algorithm_context * vessel_algo_ctx = nullptr;
  struct vessel_section_context *   vessel_sec_ctx = nullptr;
  struct vessel_context *           vessel_ctx = nullptr;

  inline api()
  {
    auto err = vessel_algorithm_context_create_from_specs(&vessel_algo_ctx,
        &DEFAULT_VESSEL_ALGOS, 1);
    if (VESSEL_ERR_SUCCESS != err) {
      throw exception{WYRD_ERR_INITIALIZATION, "Could not create vessel algorithm context."};
    }

    err = vessel_context_create(&vessel_ctx, vessel_algo_ctx);
    if (VESSEL_ERR_SUCCESS != err) {
      throw exception{WYRD_ERR_INITIALIZATION, "Could not create vessel context."};
    }

    vessel_sec_ctx = vessel_context_get_section_ctx(vessel_ctx);
  }

  inline ~api()
  {
    vessel_context_free(&vessel_ctx);
    vessel_algorithm_context_free(&vessel_algo_ctx);
  }
};

} // namespace wyrd

extern "C" {

struct wyrd_api
{
  std::shared_ptr<wyrd::api>  api;
};

} // extern "C"

#endif // guard
