/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_LIB_MERGE_STRATEGY_H
#define WYRD_LIB_MERGE_STRATEGY_H

#include <build-config.h>

#include <memory>

#include <wyrd/properties.h>

#include <liberate/serialization/varint.h>

namespace wyrd {

/**
 * Forward declarations
 */
struct property_value;
struct handle;


/**
 * Merge strategies are primarily used to set values, specifically to
 * create a new/modified property_value from some inputs.
 *
 * At the abstraction of the `property` type, this is equivalent to creating
 * a new property_value from an (optional) old value and an (optional) new
 * value. Both the old and new value are optional, because a property may
 * not yet have a value, or may be deleted.
 *
 * At the abstraction of a CRDT, a property value is created by applying
 * a change or edit to an existing value.
 *
 * The merge_strategy below is an interface which is concerned with these
 * abstractions. As the involve a strategy specific edit type, we also define
 * a base edit type for further specialization.
 *
 * Note that the merge strategy is metadata for a property as well as an edit.
 * That implies that it's possible for merge strategies to change. In some
 * cases, defaulting to the naive override strategy is the appropriate response,
 * but the interface leaves this for each strategy to decide.
 *
 * Note also that the strategy in use is an instance corresponding to the new
 * merge strategy, where that is provided. This is mostly for implementation
 * reasons, i.e. to help with the initialization of property values.
 */
struct edit
{
  /** The generic edit names the merge strategy. */
  wyrd_merge_strategy type;

  /** In adition to the value, we also need to remember the property path,
      in a normalized form. Note that the generic serialization does not use
      apply to this path, in order to permit for per-strategy optimizations
      when serializing. But in-memory, it's a generic property of edits. */
  std::string path;

  inline explicit edit(wyrd_merge_strategy _type, std::string const & _path)
    : type{_type}
    , path{_path}
  {}

  virtual ~edit() = default;
};


struct merge_strategy
{
  // XXX maybe shared_ptr is too much here.
  using edit_ptr = std::shared_ptr<edit>;
  using property_value_ptr = std::shared_ptr<property_value>;

  /**
   * Need virtual dtor
   */
  virtual ~merge_strategy() = default;

  /**
   * Apply a new property_value to an existing property_value, and produce
   * an edit as a result.
   */
  virtual std::tuple<wyrd_error_t, edit_ptr>
  apply_value(std::string const & path,
      property_value_ptr & old_value, property_value_ptr new_value,
      wyrd_merge_strategy old_strategy, wyrd_merge_strategy new_strategy) = 0;


  /**
   * Apply an edit to an existing property_value, resulting in a new
   * property value.
   */
  virtual std::tuple<wyrd_error_t, property_value_ptr>
  apply_edit(property_value_ptr & old_value, edit_ptr edit,
      wyrd_merge_strategy old_strategy) = 0;


  /**
   * Serialize an edit into a buffer, and deserialize an edit from a buffer.
   * The return value is always the bytes consumed. A return value of zero
   * indicates an issue with serializing or deserializing an edit.
   */
  virtual size_t
  serialize_edit(char * buffer, size_t bufsize, edit_ptr const & edit) = 0;
  virtual size_t
  deserialize_edit(edit_ptr & edit, char const * buffer, size_t bufsize) = 0;


  /**
   * Generic (de-)serialization functions using a handle to delegate to specific
   * strategies.
   */
  static size_t
  serialize_edit_generic(std::shared_ptr<wyrd::handle> const & handle,
      char * buffer, size_t bufsize, edit_ptr const & edit);
  static std::tuple<bool, size_t>
  deserialize_edit_generic(std::shared_ptr<wyrd::handle> const & handle,
      edit_ptr & edit, char const * buffer, size_t bufsize);
};


/**
 * Utility function for grabbing a merge strategy.
 */
std::tuple<wyrd_error_t, std::shared_ptr<merge_strategy>>
get_strategy(std::shared_ptr<handle> handle, wyrd_merge_strategy strategy);

} // namespace wyrd

#endif // guard
