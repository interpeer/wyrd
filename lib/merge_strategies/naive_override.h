/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_LIB_MERGE_STRATEGIES_NAIVE_OVERRIDE_H
#define WYRD_LIB_MERGE_STRATEGIES_NAIVE_OVERRIDE_H

#include <build-config.h>

#include <memory>

#include "../merge_strategy.h"
#include "../properties.h"
#include "../handle.h"
#include "../serialization.h"

#include <liberate/logging.h>

namespace wyrd {

/**
 * The naive override merge strategy is naive both in its ability to merge
 * values, i.e. it overrides the current value with a new value. It is also
 * naive in how these edits get serialized: the values just get written to a
 * buffer.
 */

struct naive_override_edit
  : public edit
{
  // The naive override edit simply serializes the new value, and to do so
  // well, also its type. Both are contained in a property_value, so a pointer
  // to this property_value is all that is required.
  // If the value is empty, that is equivalent to deleting the value. We do not
  // support "setting a nil value".
  merge_strategy::property_value_ptr  value;

  inline explicit naive_override_edit(merge_strategy::property_value_ptr const & _value,
      std::string const & _path)
    : edit{WYRD_MS_NAIVE_OVERRIDE, _path}
    , value{_value}
  {
  }

  virtual ~naive_override_edit() = default;
};


struct naive_override_merge_strategy
  : public merge_strategy
{
  virtual ~naive_override_merge_strategy () = default;

  virtual std::tuple<wyrd_error_t, edit_ptr>
  apply_value(
      std::string const & path,
      property_value_ptr & old_value, property_value_ptr new_value,
      wyrd_merge_strategy old_strategy, wyrd_merge_strategy new_strategy)
  {
    // Simple assign if there is no old value
    if (!old_value || WYRD_PT_UNKNOWN == old_value->type()) {
      LIBLOG_DEBUG("Previous value is not known; this is a simple assignment.");
      return do_assign(path, old_value, new_value);
    }

    // Simple delete if there is no new value
    if (!new_value || WYRD_PT_UNKNOWN == new_value->type()) {
      LIBLOG_DEBUG("New value is not known; this is a simple deletion.");
      return do_delete(path, old_value);
    }

    // At this point in time, we can't know how merge strategies may interact,
    // so we just reject anything that isn't a naive override.
    if (WYRD_MS_NAIVE_OVERRIDE != old_strategy || WYRD_MS_NAIVE_OVERRIDE != new_strategy) {
      LIBLOG_DEBUG("Strategies mismatch, abort!");
      return {WYRD_ERR_BAD_MERGE, {}};
    }

    // Since we have a naive override in both cases, we can simply assign.
    LIBLOG_DEBUG("Naively override the old value with a new value.");
    return do_assign(path, old_value, new_value);
  }



  virtual std::tuple<wyrd_error_t, property_value_ptr>
  apply_edit(property_value_ptr & old_value [[maybe_unused]],
      edit_ptr edit [[maybe_unused]],
      wyrd_merge_strategy old_strategy [[maybe_unused]])
  {
    if (WYRD_MS_NAIVE_OVERRIDE != edit->type) {
      LIBLOG_ERROR("Incompatible edit for strategy!");
      return {WYRD_ERR_INVALID_VALUE, {}};
    }

    auto typed_edit = static_cast<naive_override_edit *>(edit.get());

    // In the naive override strategy, we ignore the old strategy. We just
    // override.
    return {WYRD_ERR_SUCCESS, typed_edit->value};
  }



  virtual size_t
  serialize_edit(char * buffer, size_t bufsize, edit_ptr const & edit)
  {
    if (!buffer || !bufsize || !edit) {
      LIBLOG_ERROR("Bad parameter values.");
      return 0;
    }

    if (WYRD_MS_NAIVE_OVERRIDE != edit->type) {
      LIBLOG_ERROR("Incompatible edit for strategy!");
      return 0;
    }

    auto typed_edit = static_cast<naive_override_edit *>(edit.get());

    auto offset = buffer;
    auto remaining = bufsize;


    std::vector<char> path_buf{typed_edit->path.begin(), typed_edit->path.end()};
    auto used = util::serialize_value(offset, remaining, WYRD_PT_UTF8_STRING,
        path_buf);
    if (!used) {
      LIBLOG_ERROR("Could not serialize edit path, aborting!");
      return 0;
    }
    offset += used;
    remaining -= used;

    used = util::serialize_value(offset, remaining, typed_edit->value->type(),
        typed_edit->value->raw_memory());
    if (!used) {
      LIBLOG_ERROR("Could not serialize value, aborting!");
      return 0;
    }
    offset += used;
    remaining -= used;

    return (offset - buffer);
  }



  virtual size_t
  deserialize_edit(edit_ptr & edit, char const * buffer, size_t bufsize)
  {
    if (!buffer || !bufsize) {
      LIBLOG_ERROR("Bad parameter values.");
      return 0;
    }

    auto offset = buffer;
    auto remaining = bufsize;

    // Path
    std::vector<char> value;
    wyrd_property_type type = WYRD_PT_UNKNOWN;

    auto used = util::deserialize_value(type, value, offset, remaining);
    if (!used) {
      LIBLOG_ERROR("No path deserialized, aborting!");
      return 0;
    }
    offset += used;
    remaining -= used;

    if (WYRD_PT_UTF8_STRING != type) {
      LIBLOG_ERROR("Expected path, got " << type);
      return 0;
    }

    std::string path{value.begin(), value.end()};

    // Generic value
    value.clear();
    type = WYRD_PT_UNKNOWN;

    used = util::deserialize_value(type, value, offset, remaining);
    if (!used) {
      LIBLOG_ERROR("No path deserialized, aborting!");
      return 0;
    }
    offset += used;
    remaining -= used;

    // Create edit
    auto val = std::make_shared<wyrd::property_value>(type, value.data(), value.size());
    edit = std::make_shared<wyrd::naive_override_edit>(val, path);

    return (offset - buffer);
  }

private:

  inline std::tuple<wyrd_error_t, edit_ptr>
  do_assign(std::string const & path, property_value_ptr & old_value,
      property_value_ptr new_value)
  {
    old_value = new_value;
    return {WYRD_ERR_SUCCESS, std::make_shared<naive_override_edit>(new_value, path)};
  }


  inline std::tuple<wyrd_error_t, edit_ptr>
  do_delete(std::string const & path, property_value_ptr & old_value)
  {
    old_value.reset();
    return {WYRD_ERR_SUCCESS, std::make_shared<naive_override_edit>(old_value, path)};
  }
};


} // namespace wyrd

#endif // guard
