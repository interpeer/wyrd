/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_LIB_PROPERTY_PATH_H
#define WYRD_LIB_PROPERTY_PATH_H

#include <build-config.h>

#include <cstring>

#include <string>

#include <liberate/logging.h>

namespace wyrd {

/**
 * A property path segment.
 *
 * Property paths are dot-separated names (implying that dots themselves are
 * not valid name components). They indicate where in a nested property trie
 * a value is located, i.e. ".foo.bar" looks for a name "bar" in a container
 * that is named "foo" in the root container.
 *
 * The path_segment_iterate() function is a zero-copy path segment iteration function.
 */
struct property_path_segment
{
  char const *  start = nullptr;
  size_t        len = 0;

  inline std::string to_str() const
  {
    return std::string{start, start + len};
  }

  inline bool empty() const
  {
    return (!start || !len);
  }

  inline operator bool() const
  {
    return !empty();
  }
};

inline std::ostream &
operator<<(std::ostream & os, property_path_segment const & seg)
{
  os << "<seg:" << static_cast<void const *>(seg.start) << "+"
    << seg.len << ":";

  if (seg.start && seg.len) {
    os << seg.to_str();
  }

  os << ">";

  return os;
}


inline void
path_segment_iterate(property_path_segment * seg, char const * path,
    size_t path_len)
{
  if (!seg || !path || !path_len) {
    return;
  }

  char const * part = path;
  if (seg->start) {
    // fprintf(stderr, "%p [%zd] (%p)- %p [%zd] (%p)\n", path, path_len, path + path_len,
    //     seg->start, seg->len, seg->start + seg->len);
    part = seg->start + seg->len + 1;
    if (seg->start < path || part >= path + path_len) {
      LIBLOG_DEBUG("Part is outside of path, invalid.");
      seg->start = NULL;
      seg->len = 0;
      return;
    }
  }

  char const * dot = std::strchr(part, '.');
  while (dot) {
    size_t part_len = dot - part;
    // fprintf(stderr, "%p first part: %zd\n", dot, part_len);

    // Ignore empty parts
    if (0 == part_len) {
      part = dot + 1;

      dot = std::strchr(part, '.');
      part_len = dot - part;
      continue;
    }

    seg->start = part;
    seg->len = part_len;
    LIBLOG_DEBUG("Found part: '" << seg->to_str() << "'");
    return;
  }

  // We've advanced the part pointer past any leading dots - that's the segment.
  seg->start = part;
  seg->len = path_len - (seg->start - path);

  // Truncate trailing '\0'
  while (seg->len && !seg->start[seg->len - 1]) {
    --(seg->len);
  }
  if (!seg->len) {
    seg->start = NULL;
  }

  // Debug output
  if (!seg->start) {
    LIBLOG_DEBUG("No further part found, ending.");
  }
  else {
    LIBLOG_DEBUG("Returning remainder: '" << seg->to_str() << "'");
  }
}


inline void
path_segment_iterate(property_path_segment * seg, std::string const & path)
{
  // XXX Note that this *may* fail on some implementations, because c_str()
  //     is not guaranteed to return the same pointer every time for each
  //     invocation, even if the std::string is unmodified.
  //     That said, it definitely fails when the path string *is* being
  //     modified.
  return path_segment_iterate(seg, path.c_str(), path.size());
}




} // namespace wyrd

#endif // guard
