/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include "handle.h"
#include "properties.h"

#include "merge_strategy.h"
#include "merge_strategies/naive_override.h"

#include <liberate/logging.h>

namespace wyrd {

inline std::shared_ptr<merge_strategy>
create_strategy(wyrd_merge_strategy strategy)
{
  switch (strategy) {
    case WYRD_MS_NAIVE_OVERRIDE:
      return std::make_shared<naive_override_merge_strategy>();
      break;

    default:
      break;
  }

  return {};
}



std::tuple<wyrd_error_t, std::shared_ptr<merge_strategy>>
get_strategy(std::shared_ptr<handle> handle, wyrd_merge_strategy strategy)
{
  // If we have a handle, try to grab the strategy from there.
  if (handle) {
    auto iter = handle->api->merge_strategies.find(strategy);
    if (iter != handle->api->merge_strategies.end()) {
      if (iter->second) {
        return {WYRD_ERR_SUCCESS, iter->second};
      }
    }
  }

  // If we reached here, we need to create a strategy instance.
  auto strat = create_strategy(strategy);
  if (!strat) {
    LIBLOG_ERROR("Could not create requested merge strategy: "
        << static_cast<int>(strategy));
    return {WYRD_ERR_NOT_IMPLEMENTED, {}};
  }

  // If we have a handle, we should remember this strategy.
  if (handle) {
    handle->api->merge_strategies[strategy] = strat;
  }

  // That's it!
  return {WYRD_ERR_SUCCESS, strat};
}



size_t
merge_strategy::serialize_edit_generic(std::shared_ptr<wyrd::handle> const & handle,
  char * buffer, size_t bufsize, edit_ptr const & edit)
{
  if (!buffer || !bufsize || !handle || !handle->api || !edit) {
    LIBLOG_ERROR("Bad parameter values.");
    return 0;
  }

  // First, look up the strategy for the edit.
  auto [err, strat] = get_strategy(handle, edit->type);
  if (WYRD_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Merge strategy not supported: " << edit->type);
    return 0;
  }

  // Start serialization
  auto offset = buffer;
  auto remaining = bufsize;

  // The first thing we do is serialize the merge strategy. This is required for
  // deserialization
  auto strat_int = static_cast<liberate::types::varint>(edit->type);
  auto used = liberate::serialization::serialize_varint(offset, remaining, strat_int);
  if (!used) {
    LIBLOG_ERROR("Could not serialize merge strategy field.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // After the strategy, claim two bytes for the size.
  auto size_offset = offset;
  offset += sizeof(uint16_t);
  remaining -= sizeof(uint16_t);

  // Delegate the rest to the strategy
  used = strat->serialize_edit(offset, remaining, edit);
  if (!used) {
    LIBLOG_ERROR("Merge strategy did not seralize the edit!");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Write the size.
  if (used > std::numeric_limits<uint16_t>::max()) {
    LIBLOG_ERROR("Merge strategy wrote too large an edit, don't know what to do.");
    return 0;
  }
  uint16_t size = static_cast<uint16_t>(used);

  used = liberate::serialization::serialize_int(size_offset, sizeof(uint16_t), size);
  if (used != sizeof(uint16_t)) {
    LIBLOG_ERROR("Error writing edit size.");
    return 0;
  }
  // XXX Do *not* update offset again.


  return (offset - buffer);
}



std::tuple<bool, size_t>
merge_strategy::deserialize_edit_generic(std::shared_ptr<wyrd::handle> const & handle,
    edit_ptr & edit, char const * buffer, size_t bufsize)
{
  if (!buffer || !bufsize || !handle || !handle->api) {
    LIBLOG_ERROR("Bad parameter values.");
    return {false, 0};
  }

  auto offset = buffer;
  auto remaining = bufsize;

  // The first thing we do is deserialize the merge strategy. Based on this,
  // we can call the strategy's function to deserailize the rest.
  liberate::types::varint strat_int;
  auto used = liberate::serialization::deserialize_varint(strat_int, offset, remaining);
  if (!used) {
    LIBLOG_ERROR("Could not deserialize merge strategy field.");
    return {false, 0};
  }
  offset += used;
  remaining -= used;

  // Deserialize the size.
  uint16_t size = 0;
  used = liberate::serialization::deserialize_int(size, offset, remaining);
  if (used != sizeof(uint16_t)) {
    LIBLOG_ERROR("Error deserializing size.");
    return {false, 0};
  }
  offset += used;
  remaining -= used;

  if (!size) {
    LIBLOG_ERROR("No idea what the size is, aborting!");
    return {false, 0};
  }
  if (size > bufsize) {
    LIBLOG_ERROR("Edit size (" << size << ") is supposedly larger than the "
        "input buffer (" << bufsize << "); this must be an error!");
    return {false, 0};
  }

  // Account for header size
  size += (offset - buffer);

  // We can now look up a strategy in the handle.
  auto [err, strat] = get_strategy(handle, static_cast<wyrd_merge_strategy>(strat_int));
  if (WYRD_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Merge strategy not supported: " << strat_int);
    return {false, size};
  }

  // Delegate to the strategy
  used = strat->deserialize_edit(edit, offset, remaining);
  if (!used) {
    LIBLOG_ERROR("Merge strategy did not deseralize an edit!");
    return {false, size};
  }
  offset += used;
  remaining -= used;

  return {true, (offset - buffer)};
}


} // namespace wyrd
