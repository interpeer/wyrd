/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <wyrd/error.h>

/**
 * Stringify the symbol passed to WYRD_SPRINGIFY()
 **/
#define WYRD_STRINGIFY(n) WYRD_STRINGIFY_HELPER(n)
#define WYRD_STRINGIFY_HELPER(n) #n

/**
 * (Un-)define macros to ensure error defintions are being generated.
 **/
#define WYRD_START_ERRORS \
  static const struct { \
    char const * const name; \
    wyrd_error_t code; \
    char const * const message; \
  } _wyrd_error_table[] = {
#define WYRD_ERRDEF(name, code, desc) \
  { "WYRD_" WYRD_STRINGIFY(name), code, desc },
#define WYRD_END_ERRORS { nullptr, 0, nullptr } };

#undef WYRD_ERROR_H
#include <wyrd/error.h>



/*****************************************************************************
 * Functions
 **/

extern "C" {

char const *
wyrd_error_message(wyrd_error_t code)
{
  if (code >= WYRD_ERROR_LAST) {
    return "unidentified error";
  }

  for (size_t i = 0
      ; i < sizeof(_wyrd_error_table) / sizeof(_wyrd_error_table[0])
      ; ++i)
  {
    if (code == _wyrd_error_table[i].code) {
      return _wyrd_error_table[i].message;
    }
  }

  return "unidentified error";
}



char const *
wyrd_error_name(wyrd_error_t code)
{
  if (code >= WYRD_ERROR_LAST) {
    return "unidentified error";
  }

  for (size_t i = 0
      ; i < sizeof(_wyrd_error_table) / sizeof(_wyrd_error_table[0])
      ; ++i)
  {
    if (code == _wyrd_error_table[i].code) {
      return _wyrd_error_table[i].name;
    }
  }

  return "unidentified error";
}

} // extern "C"


/*****************************************************************************
 * Exception
 **/
#include "error.h"

#include <liberate/sys/error.h>

namespace wyrd {

namespace {

// Helper function for trying to create a verbose error message. This may fail
// if there can't be any more allocations, for example, so be extra careful.
void
combine_error(std::string & result, wyrd_error_t code, int errnum,
    std::string const & details)
{
  try {
    result = "[" + std::string{wyrd_error_name(code)} + "] ";
    result += std::string{wyrd_error_message(code)};
    if (errnum) {
      result += " // ";
      result += liberate::sys::error_message(errnum);
    }
    if (!details.empty()) {
      result += " // ";
      result += details;
    }
  } catch (...) {
    result = "Error copying error message.";
  }
}

} // anonymous namespace



exception::exception(wyrd_error_t code,
    std::string const & details /* = "" */) throw()
  : std::runtime_error("")
  , m_code(code)
{
  combine_error(m_message, m_code, 0, details);
}



exception::exception(wyrd_error_t code, int errnum,
    std::string const & details /* = "" */) throw()
  : std::runtime_error("")
  , m_code(code)
{
  combine_error(m_message, m_code, errnum, details);
}




exception::~exception() throw()
{
}



char const *
exception::what() const throw()
{
  if (m_message.empty()) {
    return wyrd_error_message(m_code);
  }
  return m_message.c_str();
}



char const *
exception::name() const throw()
{
  return wyrd_error_name(m_code);
}



wyrd_error_t
exception::code() const throw()
{
  return m_code;
}


} // namespace wyrd
