/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <wyrd/api.h>

#include "api.h"

#include <liberate/logging.h>

extern "C" {

WYRD_API wyrd_error_t
wyrd_create(struct wyrd_api ** api)
{
  if (!api) {
    return WYRD_ERR_INVALID_VALUE;
  }
  if (*api) {
    auto res = wyrd_destroy(api);
    if (WYRD_ERR_SUCCESS != res) {
      return res;
    }
  }

  *api = new wyrd_api{};
  (*api)->api = std::make_shared<wyrd::api>();
  return WYRD_ERR_SUCCESS;
}



WYRD_API wyrd_error_t
wyrd_destroy(struct wyrd_api ** api)
{
  if (!api) {
    return WYRD_ERR_INVALID_VALUE;
  }
  if (*api) {
    delete *api;
    *api = nullptr;
  }
  return WYRD_ERR_SUCCESS;
}


} // extern "C"
