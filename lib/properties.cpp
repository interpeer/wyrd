/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <wyrd/properties.h>

#include "handle.h"

extern "C" {


WYRD_API wyrd_error_t
wyrd_set_property_typed(struct wyrd_handle * handle, char const * path,
    wyrd_property_type type, wyrd_merge_strategy strategy,
    void const * value, size_t value_size,
    int override_type)
{
  if (!handle || !handle->handle || !path || !value) {
    return WYRD_ERR_INVALID_VALUE;
  }

  try {
    auto old_type = handle->handle->properties[path].type();
    if (old_type != WYRD_PT_UNKNOWN && old_type != type && !override_type) {
      LIBLOG_ERROR("Cannot override type, aborting!");
      return WYRD_ERR_BAD_PROPERTY_TYPE;
    }
    // XXX As good as it is to have properties behave like containers, we need
    //     to explicitly call .set() here in order to trigger the generation of
    //     edits, and subsequent persitence.
    handle->handle->properties[path].set(path, type, strategy, value, value_size,
        handle->handle);

  } catch (wyrd::exception const & ex) {
    LIBLOG_EXC(ex, "Unable to set property at path: " << path);
    return WYRD_ERR_BAD_PROPERTY_TYPE;
  }

  // Find callback
  auto cb_iter = handle->handle->callbacks.find(path);
  if (cb_iter != handle->handle->callbacks.end()) {
    // XXX Since this should happen *asynchronously*, we obviously can't just
    //     pass the parameters onwards, but need to pick them from iter->second
    for (auto & [callback, baton, refcount] : cb_iter->second) {
      callback(handle, path, type, value, value_size, baton);
    }
  }

  return WYRD_ERR_SUCCESS;
}



WYRD_API wyrd_error_t
wyrd_get_property(struct wyrd_handle * handle, char const * path,
    void * value, size_t * value_size)
{
  if (!handle || !handle->handle || !path || !value || !value_size) {
    return WYRD_ERR_INVALID_VALUE;
  }

  try {
    auto & prop = handle->handle->properties[path];

    switch (prop.type()) {
      case WYRD_PT_CONT_MAP:
      case WYRD_PT_CONT_LIST:
        // TODO other containers
        LIBLOG_ERROR("Cannot access container properties directly, aborting!");
        return WYRD_ERR_BAD_PROPERTY_TYPE;

      default:
        break;
    }

    // Property is found; check size.
    auto & prop_val = prop.raw_value();
    if (*value_size < prop_val.size()) {
      *value_size = prop_val.size();
      return WYRD_ERR_OUT_OF_MEMORY;
    }

    // Property size is good, copy
    std::memcpy(value, prop_val.data(), prop_val.size());
    *value_size = prop_val.size();

  } catch (wyrd::exception const & ex) {
    LIBLOG_EXC(ex, "Unable to get property at path: " << path);
    return WYRD_ERR_BAD_PROPERTY_TYPE;
  }


  return WYRD_ERR_SUCCESS;
}




WYRD_API wyrd_error_t
wyrd_get_property_type(struct wyrd_handle * handle, char const * path,
    wyrd_property_type * type)
{
  if (!handle || !handle->handle || !path || !type) {
    return WYRD_ERR_INVALID_VALUE;
  }

  try {
    auto & prop = handle->handle->properties[path];
    *type = prop.type();
  } catch (wyrd::exception const &) {
    return WYRD_ERR_PROPERTY_NOT_FOUND;
  }

  return WYRD_ERR_SUCCESS;
}




WYRD_API wyrd_error_t
wyrd_check_property_type(struct wyrd_handle * handle, char const * path,
    wyrd_property_type type)
{
  if (!handle || !handle->handle || !path) {
    return WYRD_ERR_INVALID_VALUE;
  }

  try {
    auto & prop = handle->handle->properties[path];
    if (type != prop.type()) {
      return WYRD_ERR_BAD_PROPERTY_TYPE;
    }
  } catch (wyrd::exception const &) {
    return WYRD_ERR_PROPERTY_NOT_FOUND;
  }

  return WYRD_ERR_SUCCESS;
}



WYRD_API wyrd_error_t
wyrd_get_merge_strategy(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy * strategy)
{
  if (!handle || !handle->handle || !path || !strategy) {
    return WYRD_ERR_INVALID_VALUE;
  }

  try {
    auto & prop = handle->handle->properties[path];
    *strategy = prop.strategy();
  } catch (wyrd::exception const &) {
    return WYRD_ERR_PROPERTY_NOT_FOUND;
  }

  return WYRD_ERR_SUCCESS;
}



WYRD_API wyrd_error_t
wyrd_check_merge_strategy(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy)
{
  if (!handle || !handle->handle || !path) {
    return WYRD_ERR_INVALID_VALUE;
  }

  try {
    auto & prop = handle->handle->properties[path];
    if (strategy != prop.strategy()) {
      return WYRD_ERR_BAD_PROPERTY_TYPE;
    }
  } catch (wyrd::exception const &) {
    return WYRD_ERR_PROPERTY_NOT_FOUND;
  }

  return WYRD_ERR_SUCCESS;
}



WYRD_API wyrd_error_t
wyrd_add_property_callback(struct wyrd_handle * handle, char const * path,
    wyrd_property_callback callback, void * baton)
{
  if (!handle || !handle->handle || !path || !callback) {
    return WYRD_ERR_INVALID_VALUE;
  }

  // When inserting a callback, increment the reference counter.
  auto [iter, _] = handle->handle->callbacks[path].insert(wyrd::callback_data{callback, baton});
  ++(iter->refcount);

  return WYRD_ERR_SUCCESS;
}



WYRD_API wyrd_error_t
wyrd_remove_property_callback(struct wyrd_handle * handle, char const * path,
    wyrd_property_callback callback, void * baton)
{
  if (!handle || !handle->handle || !path || !callback) {
    return WYRD_ERR_INVALID_VALUE;
  }

  auto path_iter = handle->handle->callbacks.find(path);
  if (path_iter == handle->handle->callbacks.end()) {
    return WYRD_ERR_SUCCESS;
  }

  auto iter = path_iter->second.find(wyrd::callback_data{callback, baton});
  if (iter == path_iter->second.end()) {
    return WYRD_ERR_SUCCESS;
  }

  if (iter->refcount <= 1) {
    path_iter->second.erase(iter);
    if (path_iter->second.empty()) {
      handle->handle->callbacks.erase(path_iter);
    }
  }
  else {
    --(iter->refcount);
  }

  return WYRD_ERR_SUCCESS;
}



WYRD_API wyrd_error_t
wyrd_clear_property_callback(struct wyrd_handle * handle, char const * path,
    wyrd_property_callback callback, void * baton)
{
  if (!handle || !handle->handle || !path || !callback) {
    return WYRD_ERR_INVALID_VALUE;
  }

  auto path_iter = handle->handle->callbacks.find(path);
  if (path_iter == handle->handle->callbacks.end()) {
    return WYRD_ERR_SUCCESS;
  }

  path_iter->second.erase(wyrd::callback_data{callback, baton});
  if (path_iter->second.empty()) {
    handle->handle->callbacks.erase(path_iter);
  }

  return WYRD_ERR_SUCCESS;
}


#define WYRD_TYPED_ACCESSORS(name, ctype, type) \
  WYRD_API wyrd_error_t \
  wyrd_set_property_ ## name(struct wyrd_handle * handle, char const * path, \
      wyrd_merge_strategy strategy, \
      ctype value, int override_type) \
  { \
    return wyrd_set_property_typed(handle, path, type, strategy, \
        &value, sizeof(ctype), override_type); \
  } \
  \
  WYRD_API wyrd_error_t \
  wyrd_get_property_ ## name(struct wyrd_handle * handle, char const * path, \
      ctype * value) \
  { \
    auto err = wyrd_check_property_type(handle, path, type); \
    if (WYRD_ERR_SUCCESS != err) { \
      return err; \
    } \
    size_t size = sizeof(ctype); \
    return wyrd_get_property(handle, path, value, &size); \
  }


WYRD_TYPED_ACCESSORS(uint8, uint8_t, WYRD_PT_UINT8)
WYRD_TYPED_ACCESSORS(sint8, int8_t, WYRD_PT_SINT8)
WYRD_TYPED_ACCESSORS(uint16, uint16_t, WYRD_PT_UINT16)
WYRD_TYPED_ACCESSORS(sint16, int16_t, WYRD_PT_SINT16)
WYRD_TYPED_ACCESSORS(uint32, uint32_t, WYRD_PT_UINT32)
WYRD_TYPED_ACCESSORS(sint32, int32_t, WYRD_PT_SINT32)
WYRD_TYPED_ACCESSORS(uint64, uint64_t, WYRD_PT_UINT64)
WYRD_TYPED_ACCESSORS(sint64, int64_t, WYRD_PT_SINT64)

WYRD_TYPED_ACCESSORS(float, float, WYRD_PT_FLOAT)
WYRD_TYPED_ACCESSORS(double, double, WYRD_PT_DOUBLE)



WYRD_API wyrd_error_t
wyrd_set_property_blob(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    void const * value, size_t value_size, int override_type)
{
  return wyrd_set_property_typed(handle, path, WYRD_PT_BLOB, strategy,
      value, value_size, override_type);
}


WYRD_API wyrd_error_t
wyrd_get_property_blob(struct wyrd_handle * handle, char const * path,
    void * value, size_t * value_size)
{
  auto err = wyrd_check_property_type(handle, path, WYRD_PT_BLOB);
  if (WYRD_ERR_SUCCESS != err) {
    return err;
  }
  return wyrd_get_property(handle, path, value, value_size);
}



WYRD_API wyrd_error_t
wyrd_set_property_utf8string(struct wyrd_handle * handle, char const * path,
    wyrd_merge_strategy strategy,
    char const * value, size_t value_size, int override_type)
{
  return wyrd_set_property_typed(handle, path, WYRD_PT_UTF8_STRING, strategy,
      value, value_size, override_type);
}


WYRD_API wyrd_error_t
wyrd_get_property_utf8string(struct wyrd_handle * handle, char const * path,
    char * value, size_t * value_size)
{
  auto err = wyrd_check_property_type(handle, path, WYRD_PT_UTF8_STRING);
  if (WYRD_ERR_SUCCESS != err) {
    return err;
  }
  return wyrd_get_property(handle, path, value, value_size);
}



#if 0

struct wyrd_property_iter
{
}; // FIXME


WYRD_API wyrd_error_t
wyrd_property_iter_create(struct wyrd_handle * handle,
    struct wyrd_property_iter ** iter, int depth_first)
{
  return WYRD_ERR_NOT_IMPLEMENTED;
}


WYRD_API wyrd_error_t
wyrd_property_iter_free(struct wyrd_property_iter ** iter)
{
  return WYRD_ERR_NOT_IMPLEMENTED;
}



WYRD_API wyrd_error_t
wyrd_property_iter_next(struct wyrd_property_iter * iter)
{
  return WYRD_ERR_NOT_IMPLEMENTED;
}


WYRD_API wyrd_error_t
wyrd_property_iter_valid(struct wyrd_property_iter const * iter)
{
  return WYRD_ERR_NOT_IMPLEMENTED;
}



WYRD_API char const *
wyrd_property_iter_path(struct wyrd_property_iter const * iter,
    size_t * path_size)
{
  return nullptr;
}

#endif


} // extern "C"
