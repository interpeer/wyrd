/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_LIB_CALLBACKS_H
#define WYRD_LIB_CALLBACKS_H

#include <build-config.h>

#include <wyrd/properties.h>

#include <map>
#include <unordered_set>

#include <liberate/logging.h>
#include <liberate/cpp/hash.h>
#include <liberate/cpp/operators.h>

namespace wyrd {

/**
 * Property callbacks need to be specific to a path. The nesting of properties
 * may be interesting to observe e.g. to signal parent properties when items are
 * added or deleted, but for now, we can keep it simple.
 *
 * However, we do need reference counting for when the same (callback, baton) is
 * added to a set. We do this by adding a reference counter field, but ignoring
 * it in the hash() and comparison operations.
 */
struct callback_data
  : public liberate::cpp::comparison_operators<callback_data>
{
  wyrd_property_callback  callback = nullptr;
  void *                  baton = nullptr;
  mutable size_t          refcount = 0;

  inline callback_data(wyrd_property_callback _callback,
      void * _baton)
    : callback{_callback}
    , baton{_baton}
  {
  }

  inline std::size_t hash() const
  {
    return liberate::cpp::multi_hash(callback, baton);
  }

  inline bool is_less_than(callback_data const & other) const
  {
    if (reinterpret_cast<size_t>(callback) < reinterpret_cast<size_t>(other.callback)) {
      return true;
    }
    if (baton < other.baton) {
      return true;
    }
    return false;
  }

  inline bool is_equal_to(callback_data const & other) const
  {
    return (callback == other.callback && baton == other.baton);
  }
};

using property_callback_set = std::unordered_set<callback_data>;

using property_callbacks = std::map<std::string, property_callback_set>;


} // namespace wyrd


LIBERATE_MAKE_HASHABLE(wyrd::callback_data);
LIBERATE_MAKE_COMPARABLE(wyrd::callback_data);

#endif // guard
