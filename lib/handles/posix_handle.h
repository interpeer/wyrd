/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_LIB_HANDLES_POSIX_HANDLE_H
#define WYRD_LIB_HANDLES_POSIX_HANDLE_H

#include "../handle.h"

#ifndef WYRD_POSIX
#error Not a POSIX platform!
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <cstring>

#include "../error.h"

#include <liberate/logging.h>
#include <liberate/string/hexencode.h>

namespace wyrd {

inline constexpr std::tuple<int, mode_t>
wyrd_to_posix_flags(int wyrd_flags)
{
  int posix_flags = 0;
  mode_t mode = 0;

  if (wyrd_flags & WYRD_O_RW) {
    posix_flags |= O_RDWR;
    mode |= S_IRUSR | S_IWUSR;
  }
  else if (wyrd_flags & WYRD_O_READ) {
    posix_flags |= O_RDONLY;
    mode |= S_IRUSR;
  }
  else if (wyrd_flags & WYRD_O_WRITE) {
    posix_flags |= O_WRONLY;
    mode |= S_IWUSR;
  }

  if (wyrd_flags & WYRD_O_CREATE) {
    posix_flags |= O_CREAT;
  }

  return {posix_flags, mode};
}


struct posix_range : public handle::range
{
  int     fd = -1;
  off_t   position = 0;
  char *  buf = nullptr;

  inline posix_range(int _fd, size_t _size)
    : range{_size}
    , fd{_fd}
  {
    // Get current position
    position = ::lseek(fd, 0, SEEK_CUR);

    // Seek to new position ("allocate")
    auto ret = ::lseek(fd, _size, SEEK_CUR);
    if (static_cast<off_t>(ret - _size) != position) {
      throw wyrd::exception{WYRD_ERR_FS_ERROR, "Error allocating file space!"};
    }

    buf = new char[_size];
  }

  virtual ~posix_range()
  {
    discard();
  }

  virtual char * buffer()
  {
    return buf;
  }

  virtual wyrd_error_t commit()
  {
    if (!buf) {
      LIBLOG_ERROR("Asked to commit an empty range!");
      return WYRD_ERR_INVALID_VALUE;
    }

    auto ret = ::lseek(fd, position, SEEK_SET);
    if (ret != position) {
      LIBLOG_ERROR("Could not seek to position!");
      return WYRD_ERR_FS_ERROR;
    }

    liberate::string::hexdump hd;
    LIBLOG_DEBUG("Writing " << size() << " Bytes at offset " << position << ":"
        << std::endl << hd(buf, size()));
    auto written = ::write(fd, buf, size());
    if (written != static_cast<off_t>(size())) {
      LIBLOG_ERROR("Writing range failed!");
      return WYRD_ERR_FS_ERROR;
    }

    return WYRD_ERR_SUCCESS;
  }

  virtual wyrd_error_t discard()
  {
    delete [] buf;
    buf = nullptr;
    fd = -1;
    position = 0;

    return WYRD_ERR_SUCCESS;
  }
};


struct posix_handle : public handle
{
  int fd = -1;

  inline posix_handle(std::shared_ptr<wyrd::api> const & _api,
      std::string const & _identifier,
      int _flags)
    : handle{_api, HT_POSIX, _identifier, _flags}
  {
    auto [pflags, mode] = wyrd_to_posix_flags(flags);

    int ret = open(_identifier.c_str(), pflags, mode);
    if (ret > 0) {
      fd = ret;
      return;
    }

    // TODO we should raise better error codes, but the LIBLOG_ERRNO should help
    //      with debugging. Let's skip this for later.
    LIBLOG_ERRNO("Error opening file");
    throw wyrd::exception(WYRD_ERR_FS_ERROR, "Error opening file");
  }



  virtual ~posix_handle()
  {
    close();
  }



  virtual wyrd_error_t close() final
  {
    if (fd >= 0) {
      ::close(fd);
      fd = -1;
    }
    return WYRD_ERR_SUCCESS;
  }



  virtual wyrd_error_t read(void * buf, size_t & count) final
  {
    if (fd < 0 || !buf) {
      return WYRD_ERR_INVALID_VALUE;
    }

    auto got = ::read(fd, buf, count);
    if (got < 0) {
      LIBLOG_ERRNO("Error reading file");
      got = 0;
      return WYRD_ERR_FS_ERROR;
    }

    count = got;
    return WYRD_ERR_SUCCESS;
  }



  virtual wyrd_error_t allocate(std::unique_ptr<range> & rng, size_t size)
  {
    if (rng) {
      rng->discard();
      rng.reset();
    }

    try {
      rng = std::make_unique<posix_range>(fd, size);
    } catch (wyrd::exception const & ex) {
      return ex.code();
    } catch (std::exception const & ex) {
      LIBLOG_EXC(ex, "Error allocating rnage.");
    } catch (...) {
      LIBLOG_ERROR("Error allocating rnage.");
    }

    return WYRD_ERR_SUCCESS;
  }
};

} // namespace wyrd

#endif // guard
