/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_LIB_HANDLES_VESSEL_HANDLE_H
#define WYRD_LIB_HANDLES_VESSEL_HANDLE_H

#include "../handle.h"

#include <vessel/resource.h>

#include <cstring>

#include "../error.h"

#include <liberate/logging.h>
#include <liberate/string/hexencode.h>

namespace wyrd {

// TODO
// These values are fine, but need to be standardized
static constexpr auto WYRD_TOPIC = VESSEL_TOPIC_CUSTOM_START;
static constexpr auto WYRD_SECTION = VESSEL_SECTION_CUSTOM_START;

// TODO this has to be re-thought, handed in from API or some such?
static constexpr size_t WYRD_EXTENT_MULTIPLIER = 16;

struct vessel_range : public handle::range
{
  struct vessel_resource * res = nullptr;
  struct vessel_extent * extent = nullptr;
  struct vessel_section  section{};

  inline vessel_range(struct vessel_resource * _res, struct vessel_extent * _extent, size_t _size)
    : range{_size}
    , res{_res}
    , extent{_extent}
  {
  }

  virtual ~vessel_range()
  {
    discard();
  }

  virtual char * buffer()
  {
    return static_cast<char *>(section.payload);
  }

  virtual wyrd_error_t commit()
  {
    LIBLOG_TRACE("Commit vessel range.");
    // In vessel, we edit section data directly, so there is no "commit"
    // functionality as such. However, we can commit extents, and should
    // do so here.
    if (extent) {
      auto err = vessel_resource_commit(res, extent);
      if (VESSEL_ERR_SUCCESS != err) {
        LIBLOG_DEBUG("Could not commit extent: " << vessel_error_name(err));
        return WYRD_ERR_IO_ERROR;
      }
    }

    return WYRD_ERR_SUCCESS;
  }

  virtual wyrd_error_t discard()
  {
    LIBLOG_TRACE("Discard vessel range.");
    if (extent) {
      auto err = vessel_resource_release(res, &extent);
      if (VESSEL_ERR_SUCCESS != err) {
        LIBLOG_DEBUG("Could not release extent: " << vessel_error_name(err));
        return WYRD_ERR_IO_ERROR;
      }
    }

    // TODO:
    // We need to remove the section again that we've got in this range, which
    // is not something that vessel currently supports.
    // See https://codeberg.org/interpeer/vessel/issues/19
    return WYRD_ERR_SUCCESS;
  }
};


struct vessel_handle : public handle
{
  // Set via _set_resource_option, *not* owned
  struct vessel_author *          author = nullptr;
  struct vessel_algorithm_choices algo_choices{};

  // A vessel handle is a resource+backing store combination
  struct vessel_backing_store * store = nullptr;
  struct vessel_resource * res = nullptr;

  // Current read offset
  struct vessel_extent_iterator *   ext_iter = nullptr;
  struct vessel_extent *            read_ext = nullptr;
  struct vessel_section_iterator *  sec_iter = nullptr;
  struct vessel_section             section{};
  size_t                            section_offset = 0;

  inline vessel_handle(std::shared_ptr<wyrd::api> const & _api,
      std::string const & _identifier,
      int _flags)
    : handle{_api, HT_VESSEL, _identifier, _flags}
  {
    // Create backing store
    // TODO append some extension like .wyrd?
    auto err = vessel_backing_store_create_file(&store, api->vessel_algo_ctx,
        _identifier.c_str());
    if (VESSEL_ERR_SUCCESS != err) {
      throw exception{WYRD_ERR_INITIALIZATION, "Could not create vessel backing store."};
    }

    // Create a new resource (no origin extent)
    err = vessel_resource_create(&res, api->vessel_ctx, store, nullptr);
    if (VESSEL_ERR_SUCCESS != err) {
      throw exception{WYRD_ERR_INITIALIZATION, "Could not create vessel resource store."};
    }

    // TODO this should likely be done once in the API
    err = vessel_set_section(api->vessel_sec_ctx, WYRD_SECTION, 0);
    if (VESSEL_ERR_SUCCESS != err) {
      LIBLOG_ERROR("Could not register wyrd section(s).");
      throw wyrd::exception{WYRD_ERR_FS_ERROR, "Could not register wyrd section(s)"};
    }
  }



  virtual ~vessel_handle()
  {
    vessel_extent_iterator_free(&ext_iter);
    close();
  }



  virtual wyrd_error_t close() final
  {
    vessel_resource_free(&res);
    vessel_backing_store_free(&store);
    return WYRD_ERR_SUCCESS;
  }



  virtual wyrd_error_t read(void * buf, size_t & count) final
  {
    auto vessel_err = init_read_iter();
    if (VESSEL_ERR_SUCCESS != vessel_err) {
      LIBLOG_DEBUG("Unable to initialize read iterator: " << vessel_error_name(vessel_err));
      return WYRD_ERR_IO_ERROR;
    }

    vessel_err = next_section();
    while (VESSEL_ERR_SUCCESS == vessel_err) {
      if (WYRD_TOPIC == section.topic && WYRD_SECTION == section.type) {
        copy_section_data(buf, count);
        return WYRD_ERR_SUCCESS;
      }
      else {
        skip_section();
      }

      vessel_err = next_section();
    }
    if (VESSEL_ERR_END_ITERATION == vessel_err) {
      // EOF is a success with zero bytes read
      count = 0;
      return WYRD_ERR_SUCCESS;
    }

    LIBLOG_DEBUG("Unable to proceed to next section: " << vessel_error_name(vessel_err));
    return WYRD_ERR_IO_ERROR;
  }


  inline void skip_section()
  {
    // This interacts with how next_section() works; if we set the section
    // offset to the end of the payload, this will trigger a read of the next
    // section.
    section_offset = section.payload_size;
  }


  inline void copy_section_data(void * buf, size_t & count)
  {
    // We copy whichever is smaller, the count or the remaining payload size.
    size_t to_copy = std::min(count, section.payload_size);

    std::memcpy(buf, section.payload, to_copy);

    // Advance the offset in the section *and* set the count we copied.
    section_offset += to_copy;
    count = to_copy;
  }


  inline vessel_error_t init_read_iter()
  {
    // Extent iterator
    if (ext_iter) {
      LIBLOG_TRACE("We already have an extent iterator, no need to initialize one.");
      return VESSEL_ERR_SUCCESS;
    }

    auto err = vessel_extent_iterator_create(&ext_iter, res);
    if (VESSEL_ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Error creating extent iterator: " << vessel_error_name(err));
      return err;
    }

    // Reset all else.
    vessel_resource_release(res, &read_ext);
    vessel_section_iterator_free(&sec_iter);
    section_offset = 0;

    return VESSEL_ERR_SUCCESS;
  }


  inline vessel_error_t next_section(bool force_next_extent = false)
  {
    if (section.payload && section_offset < section.payload_size) {
      // There is still data in the current section.
      LIBLOG_TRACE("Not proceeding to next section, because there is still data "
          "to consume.");
      return VESSEL_ERR_SUCCESS;
    }

    // If we need to grab a section, we need to get a section iterator.
    // And in order to have a section iterator, we need to have an extent.
    vessel_error_t err = VESSEL_ERR_SUCCESS;

    if (!sec_iter) {
      if (!read_ext || force_next_extent) {
        err = vessel_extent_iterator_next(&read_ext, ext_iter);
        if (VESSEL_ERR_SUCCESS != err) {
          LIBLOG_DEBUG("Error advancing extent iterator: " << vessel_error_name(err));
          return err;
        }
      }

      err = vessel_section_iterator_create(&sec_iter, api->vessel_sec_ctx,
          read_ext);
      if (VESSEL_ERR_SUCCESS != err) {
        LIBLOG_DEBUG("Error creating section iterator: " << vessel_error_name(err));
        return err;
      }
    }

    // Wether or not the section is fully returned or not initialized,
    // we need to grab the next section
    err = vessel_section_iterator_next(&section, sec_iter);
    if (VESSEL_ERR_END_ITERATION == err) {
      // TODO next extent - ideally re-use same function, but that may need
      //      some extra flags
      vessel_section_iterator_free(&sec_iter);
      return next_section(true);
    }
    else if (VESSEL_ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Error advancing section iterator: " << vessel_error_name(err));
      return err;
    }

    // We have a new section, so the section offset becomes zero.
    section_offset = 0;
    return VESSEL_ERR_SUCCESS;
  }


  virtual wyrd_error_t allocate(std::unique_ptr<range> & rng, size_t size)
  {
    if (rng) {
      rng->discard();
      rng.reset();
    }

    // Validate state
    if (!author) {
      LIBLOG_ERROR("Cannot allocate from a vessel resource without an "
          "author.");
      return WYRD_ERR_INVALID_VALUE;
    }

    if (!algo_choices.version_tag) {
      LIBLOG_ERROR("Cannot allocate from a vessel resource without a "
          "set of algorithm choices.");
      return WYRD_ERR_INVALID_VALUE;
    }

    // Make space for the buffer
    vessel_extent * extent = nullptr;
    size_t to_write = size;

    auto err = vessel_resource_make_space_for_section(res,
        &extent, &algo_choices,
        WYRD_SECTION, WYRD_TOPIC,
        size, &to_write,
        WYRD_EXTENT_MULTIPLIER,
        author, VESSEL_MF_NONE);
    if (VESSEL_ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Unable to make space for section of size " << size
          << ": " << vessel_error_name(err));
      return WYRD_ERR_IO_ERROR;
    }

    rng = std::make_unique<vessel_range>(res, extent, size);
    auto rng_cast = reinterpret_cast<vessel_range *>(rng.get());

    // Add resource
    err = vessel_resource_add_return_section(&(rng_cast->section), res, extent,
        WYRD_SECTION, WYRD_TOPIC,
        nullptr, size);
    if (VESSEL_ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Unable to add section of size " << size << ": "
          << vessel_error_name(err));
      rng->discard();
      rng.reset();
      return WYRD_ERR_IO_ERROR;
    }

    if (rng_cast->section.payload_size != size) {
      LIBLOG_ERROR("Created range, but with off size.");
      return WYRD_ERR_IO_ERROR;
    }

    return WYRD_ERR_SUCCESS;
  }
};

} // namespace wyrd

#endif // guard
