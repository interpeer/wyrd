/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <wyrd/handle.h>

#include <liberate/logging.h>

#include "api.h"
#include "handle.h"

namespace wyrd {

WYRD_PRIVATE
inline size_t
parse_edits_from_buffer(std::shared_ptr<handle> _handle, std::vector<char> const & buffer, size_t bufsize,
    bool skip_unknown)
{
  if (!bufsize) {
    return 0;
  }

  // We use an explicit buffer size because the vector may not be used in its
  // entirety.
  size_t remaining = bufsize;
  auto offset = buffer.data();

  while (remaining) {
    merge_strategy::edit_ptr edit;
    auto [success, used] = merge_strategy::deserialize_edit_generic(_handle, edit, offset,
        remaining);
    if (!success) {
      if (used && skip_unknown) {
        LIBLOG_WARN("Skipping unreadable edit of size " << used);
        offset += used;
        remaining -= used;
        continue;
      }

      LIBLOG_ERROR("Aborting read, there is an unrecoverable problem.");
      break;
    }
    offset += used;
    remaining -= used;

    if (edit) {
      // Given an edit, we apply it to the handle's properties via the edit's
      // merge strategy.
      auto [err, strat] = get_strategy(_handle, edit->type);
      if (WYRD_ERR_SUCCESS != err) {
        LIBLOG_WARN("Found edit with unknown strategy " << edit->type << ", skipping!");
      }
      else {
        // We need to find the property value that belongs to this edit.
        auto & prop = _handle->properties[edit->path];
        auto [err2, new_value] = strat->apply_edit(prop.value(), edit, prop.strategy());
        if (WYRD_ERR_SUCCESS != err2) {
          LIBLOG_WARN("Could not apply edit: " << wyrd_error_name(err2) << " // " << wyrd_error_message(err2));
        }
        else {
          // Set the property value from the strategy's application.
          _handle->properties[edit->path].set_value_and_strategy(new_value, edit->type);
        }
      }
    }

  }

  return offset - buffer.data();
}




WYRD_PRIVATE
inline wyrd_error_t
initialize_properties(std::shared_ptr<handle> _handle, bool skip_unknown)
{
  if (!_handle) {
    return WYRD_ERR_INVALID_VALUE;
  }

  // We need to allocate some buffer for serialization, but the current APIs
  // do not provide feedback on required size. TODO this could be improved.
  // As a consequence, we use a configurable "maximum" value.
  std::vector<char> buffer;
  buffer.resize(WYRD_EDIT_BUFFER_SIZE);

  // Keep reading.
  size_t offset = 0;
  do {
    size_t read = buffer.size() - offset;
    auto err = _handle->read(buffer.data() + offset, read);
    if (WYRD_ERR_SUCCESS != err) {
      return err;
    }
    if (!read) {
      // EOF
      break;
    }

    liberate::string::hexdump hd;
    LIBLOG_DEBUG("Buffer from before:"
        << std::endl << hd(buffer.data(), offset)
        << "After read of " << read << " Bytes:"
        << std::endl << hd(buffer.data() + offset, read));

    // Keep reading from the buffer.
    auto max = offset + read;
    auto parsed = parse_edits_from_buffer(_handle, buffer, max, skip_unknown);

    if (parsed < max) {
      // We have to move the parsed amount to the start of the buffer, and
      // set the offset to after it. The amount of data we need to move is the
      // difference between the maximum amount in the buffer and the parsed
      // part. This then also becomes the offset for further reading.
      offset = max - parsed;
      std::memmove(buffer.data(), buffer.data() + parsed, offset);
    }
  } while (true);

  return WYRD_ERR_SUCCESS;
}



WYRD_PRIVATE wyrd_error_t
persist_edit(std::shared_ptr<handle> _handle, std::shared_ptr<edit> _edit)
{
  if (!_handle || !_edit) {
    return WYRD_ERR_INVALID_VALUE;
  }

  // We need to allocate some buffer for serialization, but the current APIs
  // do not provide feedback on required size. TODO this could be improved.
  // As a consequence, we use a configurable "maximum" value.
  std::vector<char> buffer;
  buffer.resize(WYRD_EDIT_BUFFER_SIZE);
  auto used = merge_strategy::serialize_edit_generic(_handle,
      buffer.data(), buffer.size(), _edit);
  if (!used) {
    LIBLOG_ERROR("Could not serialize edit!");
    return WYRD_ERR_SERIALIZATION;
  }
  buffer.resize(used);

  // Once serialized, we can request that the handle allocate an appropriate
  // part of memory.
  std::unique_ptr<handle::range> range;
  auto err = _handle->allocate(range, buffer.size());
  if (WYRD_ERR_SUCCESS != err || !range || range->size() != buffer.size()) {
    LIBLOG_ERROR("Could not allocate handle space!");
    // TODO generic I/O error
    return WYRD_ERR_FS_ERROR;
  }

  // Write memory and commit the range.
  std::memcpy(range->buffer(), buffer.data(), range->size());

  liberate::string::hexdump hd;
  LIBLOG_DEBUG("Buffer copied to range::"
      << std::endl << hd(range->buffer(), range->size()));


  err = range->commit();
  if (WYRD_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Could not commit handle I/O!");
    // TODO generic I/O error
    return WYRD_ERR_FS_ERROR;
  }

  // All done
  return WYRD_ERR_SUCCESS;
}


} // namespace wyrd



extern "C" {

WYRD_API wyrd_error_t
wyrd_open_file(struct wyrd_api * api, struct wyrd_handle ** handle,
    char const * filename, int flags)
{
#ifdef WYRD_POSIX
  if (!api || !api->api || !handle || !filename) {
    return WYRD_ERR_INVALID_VALUE;
  }
  if (*handle) {
    auto res = wyrd_close(handle);
    if (WYRD_ERR_SUCCESS != res) {
      return res;
    }
  }

  *handle = new wyrd_handle{};
  try {
    (*handle)->handle = std::make_shared<wyrd::posix_handle>(api->api,
        filename, flags);
  } catch (wyrd::exception const & ex) {
    return ex.code();
  } catch (std::exception const & ex) {
    LIBLOG_EXC(ex, "Unknown exception caught, aborting!");
  } catch (...) {
    LIBLOG_ERROR("Unknown exception caught, aborting!");
    return WYRD_ERR_UNEXPECTED;
  }

  return initialize_properties((*handle)->handle,
      flags & WYRD_O_SKIP_UNKNOWN);
#else
  return WYRD_ERR_NOT_IMPLEMENTED;
#endif
}



WYRD_API wyrd_error_t
wyrd_open_resource(struct wyrd_api * api,
    struct wyrd_handle ** handle,
    char const * identifier, int flags)
{
  if (!api || !api->api || !handle || !identifier) {
    return WYRD_ERR_INVALID_VALUE;
  }
  if (*handle) {
    auto res = wyrd_close(handle);
    if (WYRD_ERR_SUCCESS != res) {
      return res;
    }
  }

  *handle = new wyrd_handle{};
  try {
    (*handle)->handle = std::make_shared<wyrd::vessel_handle>(api->api,
        identifier, flags);
  } catch (wyrd::exception const & ex) {
    return ex.code();
  } catch (std::exception const & ex) {
    LIBLOG_EXC(ex, "Unknown exception caught, aborting!");
  } catch (...) {
    LIBLOG_ERROR("Unknown exception caught, aborting!");
    return WYRD_ERR_UNEXPECTED;
  }

  return initialize_properties((*handle)->handle,
      flags & WYRD_O_SKIP_UNKNOWN);
}



WYRD_API wyrd_error_t
wyrd_set_resource_option(struct wyrd_handle * handle,
    wyrd_resource_options option, void * value)
{
  if (!handle || !handle->handle) {
    return WYRD_ERR_INVALID_VALUE;
  }

  if (wyrd::HT_VESSEL != handle->handle->type) {
    return WYRD_ERR_INVALID_VALUE;
  }

  auto vh = static_cast<wyrd::vessel_handle *>(handle->handle.get());

  switch (option) {
    case WYRD_RO_AUTHOR:
      vh->author = static_cast<vessel_author *>(value);
      break;

    case WYRD_RO_ALGORITHMS:
      vh->algo_choices = *static_cast<vessel_algorithm_choices *>(value);
      break;

    default:
      // Invalid option
      return WYRD_ERR_INVALID_VALUE;
  }

  return VESSEL_ERR_SUCCESS;
}




WYRD_API wyrd_error_t
wyrd_close(struct wyrd_handle ** handle)
{
  if (!handle) {
    return WYRD_ERR_INVALID_VALUE;
  }

  if (!*handle) {
    return WYRD_ERR_SUCCESS;
  }

  wyrd_error_t ret = WYRD_ERR_SUCCESS;
  if ((*handle)->handle) {
    ret = (*handle)->handle->close();
  }

  delete *handle;
  *handle = nullptr;

  return ret;
}


} // extern "C"
