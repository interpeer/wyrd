/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_LIB_PROPERTIES_H
#define WYRD_LIB_PROPERTIES_H

#include <build-config.h>

#include <wyrd/properties.h>

#include <map>
#include <vector>
#include <string>
#include <list>
#include <unordered_set>
#include <unordered_map>
#include <cstring>
#include <functional>
#include <memory>

#ifdef WYRD_HAVE_ICU
#include <unicode/ucsdet.h>
#include <unicode/ucnv.h>
#endif

#include <liberate/logging.h>

#include "error.h"
#include "property_path.h"
#include "merge_strategy.h"

namespace wyrd {

/**
 * Forward declarations
 */
struct handle;

WYRD_PRIVATE wyrd_error_t
persist_edit(std::shared_ptr<handle> _handle, std::shared_ptr<edit> _edit);

namespace {

/**
 * Persist an edit if there is a handle; but if there is no handle, treat this
 * as success. This is for internal testing purposes, so there will be an error
 * log output.
 */
inline wyrd_error_t
maybe_persist_edit(std::shared_ptr<handle> & _handle, std::shared_ptr<edit> & _edit)
{
  if (!_handle) {
    LIBLOG_ERROR("There is no handle for the property; treating this as a test "
        "case!");
    return WYRD_ERR_SUCCESS;
  }
  return persist_edit(_handle, _edit);
}


} // anonymous namespace


/**
 * We want to keep property values of an indeterminate type, and use them as
 * C++ native types (more or less). In order to let the compiler figure out
 * whether it's better to treat this as a reference, const reference or value,
 * but also in order to provide run-time feedback (exceptions) for bad
 * conversions, we provide a property_mapping.
 *
 * The mapping references a property, and provides some default
 * conversions. This mapping then gets specialized for each type we recognize.
 */
struct property;


template <typename T>
struct property_mapping
{
  static constexpr int type = WYRD_PT_UNKNOWN;

  inline property_mapping(property const &)
  {
  }

  inline operator T & ()
  {
    throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "This conversion is not implemented."};
  }

  inline operator T const & () const
  {
    throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "This conversion is not implemented."};
  }

  inline operator T ()
  {
    throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "This conversion is not implemented."};
  }
};


/**
 * The property_value class erases the value type by managing a union of
 * pointers. In order to do so, it must also know the property type.
 *
 * Note that the value is non-copyable to make memory management simpler.
 * It's used via a pointer, which makes this fine.
 */
struct property_value
{
public:
  using property_ptr = std::shared_ptr<property>;
  using linear_list_type = std::list<property_ptr>;
  using mapping_type = std::unordered_map<std::string, property_ptr>;
  using raw_memory_type = std::vector<char>;

  inline property_value(wyrd_property_type _type, void const * _value,
      size_t _value_size)
    : property_value{_type}
  {
    if (_value && _value_size
        && m_type != WYRD_PT_CONT_MAP
        && m_type != WYRD_PT_CONT_LIST)
    {
      m_variant.raw_memory->resize(_value_size);
      std::memcpy(m_variant.raw_memory->data(), _value, _value_size);
    }
  }

  inline explicit property_value(wyrd_property_type _type)
    : m_type{_type}
  {
    switch (m_type) {
      case WYRD_PT_UNKNOWN:
        // No allocation
        break;

      case WYRD_PT_CONT_MAP:
        m_variant.map = new mapping_type{};
        break;

      case WYRD_PT_CONT_LIST:
        m_variant.list = new linear_list_type{};
        break;

      default:
        m_variant.raw_memory = new raw_memory_type{};
        break;
    }
  }

  inline ~property_value()
  {
    switch (m_type) {
      case WYRD_PT_UNKNOWN:
        // No allocation made
        break;

      case WYRD_PT_CONT_MAP:
        delete m_variant.map;
        break;

      case WYRD_PT_CONT_LIST:
        delete m_variant.list;
        break;

      default:
        delete m_variant.raw_memory;
        break;
    }

    m_variant.raw_memory = nullptr;
  }


  inline wyrd_property_type type() const
  {
    return m_type;
  }


  inline raw_memory_type &
  raw_memory()
  {
    null_check();

    switch (m_type) {
      case WYRD_PT_CONT_MAP:
      case WYRD_PT_CONT_LIST:
        throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "This conversion is not implemented."};

      default:
        break;
    }

    return *(m_variant.raw_memory);
  }


  inline raw_memory_type const &
  raw_memory() const
  {
    null_check();

    switch (m_type) {
      case WYRD_PT_CONT_MAP:
      case WYRD_PT_CONT_LIST:
        throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "This conversion is not implemented."};

      default:
        break;
    }

    return *(m_variant.raw_memory);
  }


  inline linear_list_type &
  list()
  {
    null_check();

    switch (m_type) {
      case WYRD_PT_CONT_LIST:
        break;

      default:
        throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "This conversion is not implemented."};
    }

    return *(m_variant.list);
  }


  inline linear_list_type const &
  list() const
  {
    null_check();

    switch (m_type) {
      case WYRD_PT_CONT_LIST:
        break;

      default:
        throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "This conversion is not implemented."};
    }

    return *(m_variant.list);
  }


  inline mapping_type &
  map()
  {
    null_check();

    switch (m_type) {
      case WYRD_PT_CONT_MAP:
        break;

      default:
        throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "This conversion is not implemented."};
    }

    return *(m_variant.map);
  }


  inline mapping_type const &
  map() const
  {
    null_check();

    switch (m_type) {
      case WYRD_PT_CONT_MAP:
        break;

      default:
        throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "This conversion is not implemented."};
    }

    return *(m_variant.map);
  }


private:

  inline property_value(property_value const &) = delete;
  inline property_value & operator=(property_value const &) = delete;

  inline void null_check() const
  {
    if (!m_variant.raw_memory) {
      throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Cannot dereference uninitialized property!"};
    }
  }

  union value_variant
  {
    raw_memory_type *     raw_memory = nullptr;
    linear_list_type *    list;
    mapping_type *        map;
  };

  wyrd_property_type  m_type  = WYRD_PT_UNKNOWN;
  value_variant       m_variant;
};




/**
 * Properties have a type and value, but also a merge strategy.
 *
 * This type encapsulates the property as a variant type that can behave like
 * a primitive type, a list, a map, or other specializations.
 *
 * Note that properties shallow-copy, i.e. they share a common value.
 */
struct property
{
public:
  // Type aliases
  using property_ptr = property_value::property_ptr;
  using linear_list_type = property_value::linear_list_type;
  using mapping_type = property_value::mapping_type;
  using raw_memory_type = property_value::raw_memory_type;

  /**
   * We use a shared_ptr to property_value so properties become copyable;
   * really, it means they do not copy, but refer to the same value.
   */
  using value_ptr = std::shared_ptr<property_value>;


  /**
   * Basic constructors and default value type implementations.
   */
  inline property(wyrd_property_type _type,
      wyrd_merge_strategy _strategy = WYRD_MS_NAIVE_OVERRIDE,
      void const * _value = nullptr,
      size_t _value_size = 0)
    : m_strategy{_strategy}
    , m_value{std::make_shared<property_value>(_type, _value, _value_size)}
  {
  }

  template <typename T>
  inline property(wyrd_merge_strategy _strategy, T const & _value,
      std::shared_ptr<handle> _handle = {})
  {
    // FIXME what path here?
    set("", _strategy, _value, _handle);
  }

  inline ~property() = default;

  inline property(property const &) = default;
  inline property(property && other) = default;

  inline property & operator=(property const &) = default;
  inline property & operator=(property &&) = default;

  /**
   * Metadata accessors
   */
  inline wyrd_property_type type() const
  {
    if (!m_value) {
      return WYRD_PT_UNKNOWN;
    }
    return m_value->type();
  }

  inline wyrd_merge_strategy strategy() const
  {
    return m_strategy;
  }


  /**
   * Direct value access
   */
  inline value_ptr & value()
  {
    return m_value;
  }


  inline void set_value_and_strategy(value_ptr const & value, wyrd_merge_strategy _strategy)
  {
    m_strategy = _strategy;
    m_value = value;
  }


  /**
   * Data accessors
   */
  inline raw_memory_type & raw_value()
  {
    if (!m_value) {
      throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Cannot dereference empty property!"};
    }
    return m_value->raw_memory();
  }

  inline raw_memory_type const & raw_value() const
  {
    if (!m_value) {
      throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Cannot dereference empty property!"};
    }
    return m_value->raw_memory();
  }


  /**
   * Value setting
   */
  inline void
  set(char const * path,
      wyrd_property_type _type, wyrd_merge_strategy _strategy,
      void const * _value, size_t _value_size,
      std::shared_ptr<handle> _handle)
  {
    auto [err, strat] = get_strategy(_handle, _strategy);
    if (WYRD_ERR_SUCCESS != err) {
      throw wyrd::exception{err, "No strategy available!"};
    }

    auto new_val = std::make_shared<property_value>(_type, _value, _value_size);
    auto [err2, edit] = strat->apply_value(path, m_value, new_val, m_strategy, _strategy);
    if (WYRD_ERR_SUCCESS != err2) {
      throw wyrd::exception{err, "Unable to apply new property value!"};
    }

    m_strategy = _strategy;

    err = maybe_persist_edit(_handle, edit);
    if (WYRD_ERR_SUCCESS != err) {
      throw wyrd::exception{err, "Could not persist edit!"};
    }
  }

  template <typename T>
  inline void
  set(char const * path [[maybe_unused]],
      wyrd_merge_strategy _strategy [[maybe_unused]],
      T const & _value [[maybe_unused]],
      std::shared_ptr<handle> _handle [[maybe_unused]] = {})
  {
    throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "This conversion is not implemented."};
  }


  /**
   * List access
   */
  inline void
  append(wyrd_property_type _type, wyrd_merge_strategy _strategy,
      void const * _value, size_t _value_size)
  {
    if (!m_value || WYRD_PT_CONT_LIST != m_value->type()) {
      throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Cannot append to a non-list type."};
    }

    // Use raw constructor
    auto prop = std::make_shared<property>(_type, _strategy, _value, _value_size);
    m_value->list().push_back(std::move(prop));
  }


  template <typename T>
  inline void
  append(wyrd_merge_strategy _strategy, T const & _value)
  {
    if (!m_value || WYRD_PT_CONT_LIST != m_value->type()) {
      throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Cannot append to a non-list type."};
    }

    // Re-use specialized constructor
    auto prop = std::make_shared<property>(_strategy, _value);
    m_value->list().push_back(std::move(prop));
  }


  inline void
  append(property const & prop)
  {
    m_value->list().push_back(std::make_shared<property>(prop));
  }



  inline property &
  operator[](size_t index)
  {
    if (!m_value || WYRD_PT_CONT_LIST != m_value->type()) {
      throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Cannot append to a non-list type."};
    }

    return at(m_value->list(), index);
  }


  inline property const &
  operator[](size_t index) const
  {
    if (!m_value || WYRD_PT_CONT_LIST != m_value->type()) {
      throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Cannot append to a non-list type."};
    }

    return at(m_value->list(), index);
  }


  /**
   * Map access
   */
  inline void
  insert(std::string const & key,
      wyrd_property_type _type, wyrd_merge_strategy _strategy,
      void const * _value, size_t _value_size,
      std::shared_ptr<handle> _handle = {})
  {
    // FIXME maybe path, not key?
    _nested_access(*this, key.c_str(), key.size(), true)
      .set(key.c_str(), _type, _strategy, _value, _value_size, _handle);
  }


  template <typename T>
  inline void
  insert(std::string const & key, wyrd_merge_strategy _strategy, T const & _value,
      std::shared_ptr<handle> _handle = {})
  {
    // FIXME maybe path, not key?
    _nested_access(*this, key.c_str(), key.size(), true)
      .set(key.c_str(), _strategy, _value, _handle);
  }


  inline property &
  operator[](std::string const & key)
  {
    return _nested_access(*this, key.c_str(), key.size(), true);
  }


  inline property const &
  operator[](std::string const & key) const
  {
    return _nested_access(*this, key.c_str(), key.size(), false);
  }



  /**
   * The as<foo>() function returns a property_mapping<foo>, which the compiler
   * can then use to get a reference or copy of the contained value.
   *
   * This function also contains the runtime type check.
   */
  template <typename T>
  inline property_mapping<T>
  as() const
  {
    if (!m_value || property_mapping<T>::type != m_value->type()) {
      throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Bad conversion requested."};
    }
    return property_mapping<T>{*this};
  }


  template <typename T>
  inline bool
  operator==(T const & other) const
  {
    return as<T>() == other;
  }


private:
  template <typename T>
  friend struct property_mapping;

  wyrd_merge_strategy m_strategy;
  mutable value_ptr   m_value = {};

  // The at() function makes it simpler to provide a different version for
  // different list types, in case they change. Also, we can cast constness
  // away in the calling.
  inline static
  property &
  at(std::list<property_ptr> & list, size_t index)
  {
    if (index >= list.size()) {
      throw wyrd::exception{WYRD_ERR_PROPERTY_NOT_FOUND, "Index out of range."};
    }

    // Lists need to iterate linearly, unfortunately.
    auto iter = list.begin();
    for ( ; iter != list.end() && index ; iter++, index--)
    {
    }

    return *(iter->get());
  }

  // Similarly, an at() function accepting a string key does much the same
  // thing.
  inline static
  property &
  at(std::unordered_map<std::string, property_ptr> & map, std::string const & key, bool create)
  {
    auto iter = map.find(key);
    if (iter == map.end()) {
      if (!create) {
        throw wyrd::exception{WYRD_ERR_PROPERTY_NOT_FOUND, "Key not in map."};
      }

      auto [iter2, success] = map.insert({key, std::make_shared<property>(WYRD_PT_UNKNOWN)});
      if (!success) {
        throw wyrd::exception{WYRD_ERR_PROPERTY_NOT_FOUND, "Key not in map."};
      }
      return *(iter2->second.get());
    }

    return *(iter->second.get());
  }



  inline bool
  _check_or_create_map(bool create) const
  {
    if (!m_value) {
      if (create) {
        m_value = std::make_shared<property_value>(WYRD_PT_CONT_MAP);
        return true;
      }
      return false;
    }

    switch (m_value->type()) {
      case WYRD_PT_CONT_MAP:
        return true;

      case WYRD_PT_UNKNOWN:
        if (create) {
          m_value = std::make_shared<property_value>(WYRD_PT_CONT_MAP);
          return true;
        }
        return false;

      default:
        break;
    }

    throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Cannot insert into a non-map type."};
  }


  inline bool
  _check_list() const
  {
    return m_value && WYRD_PT_CONT_LIST == m_value->type();
  }


  template <typename propertyT>
  static inline propertyT &
  _nested_access(propertyT & prop, char const * path, size_t size, bool create)
  {
    property_path_segment seg;
    return _nested_access(prop, seg, path, size, create);
  }


  template <typename propertyT>
  static inline propertyT &
  _nested_access(propertyT & prop, property_path_segment & seg, char const * path, size_t size, bool create)
  {
    path_segment_iterate(&seg, path, size);

    if (seg.empty()) {
      return prop;
    }

    bool do_create = create;

    try {
      std::size_t pos = 0;
      std::size_t idx = std::stoll(seg.to_str(), &pos);

      // If the entire segment was consumed in the conversion, we can treat it
      // as an array index.
      if (pos == seg.len) {
        // The value has to be a list. If it's not, we can fall back to trying
        // to access the element as a map, but creating empty maps is likely
        // wrong.
        if (prop._check_list()) {
          return _nested_access(prop.at(prop.m_value->list(), idx),
              seg, path, size, create
          );
        }
        else {
          do_create = false;
        }
      }

    } catch (std::exception const &) {
    }

    // Treat seg as a string key into a map.
    if (!prop._check_or_create_map(do_create)) {
      throw wyrd::exception{WYRD_ERR_PROPERTY_NOT_FOUND, "Key not in map."};
    }

    return _nested_access(
        prop.at(prop.m_value->map(), seg.to_str(), create),
        seg, path, size, create
    );
  }
};


template <typename T>
inline bool
operator==(T const & left, property const & right)
{
  return right.as<T>() == left;
}



/**
 * We need to provide a set function for the property that initializes the
 * object well. In most cases, the default of copying the value's
 * memory is sufficient, but we need to provide a runtime type as well.
 */
#define WYRD_PROPERTY_DEFAULT_SET(cpp_type, property_type) \
  template <> \
  inline void \
  property::set<cpp_type>( \
      char const * path, \
      wyrd_merge_strategy _strategy, \
      cpp_type const & _value, \
      std::shared_ptr<handle> _handle /* = {} */) \
  { \
    set(path, property_type, _strategy, &_value, sizeof(cpp_type), _handle); \
  }



/**
 * Similarly, property_mapping needs to hold a reference to a property
 * and ctors to initialize this.
 */
#define WYRD_PROPERTY_MAPPING_DEFAULTS \
  property & prop; \
  inline explicit property_mapping(property & _prop) \
    : prop{_prop} \
  { \
  } \
  inline explicit property_mapping(property const & _prop) \
    : prop{const_cast<property &>(_prop)} \
  { \
  }


/**
 * Putting this together in the most common type of specializiation for
 * primitive types.
 *
 * This macro assumes the value's memory has been copied and provides
 * reference and const reference access.
 */
#define WYRD_PROPERTY_SPECIALIZATION(cpp_type, property_type) \
  WYRD_PROPERTY_DEFAULT_SET(cpp_type, property_type) \
  template <> struct property_mapping<cpp_type> { \
    WYRD_PROPERTY_MAPPING_DEFAULTS \
    \
    static constexpr int type = property_type; \
    \
    inline operator cpp_type & () { \
      return *static_cast<cpp_type *>(static_cast<void *>(prop.raw_value().data())); \
    } \
    inline operator cpp_type const & () const { \
      return *static_cast<cpp_type const *>(static_cast<void const *>(prop.raw_value().data())); \
    } \
  };


WYRD_PROPERTY_SPECIALIZATION(bool,      WYRD_PT_BOOL);
WYRD_PROPERTY_SPECIALIZATION(uint8_t,   WYRD_PT_UINT8);
WYRD_PROPERTY_SPECIALIZATION(int8_t,    WYRD_PT_SINT8);
WYRD_PROPERTY_SPECIALIZATION(uint16_t,  WYRD_PT_UINT16);
WYRD_PROPERTY_SPECIALIZATION(int16_t,   WYRD_PT_SINT16);
WYRD_PROPERTY_SPECIALIZATION(uint32_t,  WYRD_PT_UINT32);
WYRD_PROPERTY_SPECIALIZATION(int32_t,   WYRD_PT_SINT32);
WYRD_PROPERTY_SPECIALIZATION(uint64_t,  WYRD_PT_UINT64);
WYRD_PROPERTY_SPECIALIZATION(int64_t,   WYRD_PT_SINT64);


WYRD_PROPERTY_SPECIALIZATION(float,     WYRD_PT_FLOAT);
WYRD_PROPERTY_SPECIALIZATION(double,    WYRD_PT_DOUBLE);

// TODO more specializations

/**
 * Explicit specializations
 */

namespace detail {

template <typename T>
struct guard
{
  using close_func = std::function<void (T *)>;

  T *         value;
  close_func  close;


  inline guard(T * _value, close_func _close)
    : value{_value}
    , close{_close}
  {
  }

  inline ~guard()
  {
    close(value);
  }

  inline operator T*()
  {
    return value;
  }
};


template <typename charT>
inline void
simple_copy(property_value::raw_memory_type & result, std::basic_string<charT> const & input)
{
  result.resize(input.size());
  std::memcpy(&result[0], input.c_str(), result.size());
}


template <typename charT>
inline bool
convert_to_utf8(property_value::raw_memory_type & result, std::basic_string<charT> const & input)
{
#ifdef WYRD_HAVE_ICU
  static std::string UTF8_NAME = "UTF-8";

  // Detect the character set. If it's UTF-8, we're good! If it is not, however...
  UErrorCode status = U_ZERO_ERROR;
  guard<UCharsetDetector> detector{ucsdet_open(&status), ucsdet_close};

  ucsdet_setText(detector, input.c_str(), input.length(), &status);

  int32_t num_matches = 0;
  std::string from_page;
  auto matches = ucsdet_detectAll(detector, &num_matches, &status);
  if (!matches) {
    LIBLOG_WARN("No charset detected, copying verbatim!");
    simple_copy(result, input);
    return false;
  }
  else {
    for (int32_t i = 0 ; i < num_matches ; ++i) {
      auto name = ucsdet_getName(matches[i], &status);
      if (UTF8_NAME == name) {
        simple_copy(result, input);
        return true;
      }
      else {
        from_page = name;
        break;
      }
    }
  }

  // Copy if no good code page detection could be made
  if (from_page.empty()) {
    LIBLOG_WARN("No good charset match, copying verbatim!");
    simple_copy(result, input);
    return false;
  }

  // Convert to UTF-8; if we've reached here, we need to
  status = U_ZERO_ERROR;

  // We should try a few times to convert, growing the output each time.
  static constexpr auto RESIZE_FACTOR = 2;
  static constexpr auto MAX_ATTEMPTS = 3;

  result.resize(input.size() * RESIZE_FACTOR);
  for (auto i = 0 ; i < MAX_ATTEMPTS ; ++i) {
    auto used = ucnv_convert(UTF8_NAME.c_str(), from_page.c_str(),
        &result[0], result.size(), input.c_str(), input.size(),
        &status);
    if (!U_FAILURE(status)) {
      result.resize(used);
      return true;
    }

    status = U_ZERO_ERROR;
    result.resize(result.size() * RESIZE_FACTOR);
  }

  // Finally, simply copy again...
  LIBLOG_WARN("Conversion to UTF-8 failed, copying verbatim!");
  simple_copy(result, input);
  return false;

#else
  LIBLOG_WARN("No UTF-8 checks or conversion performed!");
  simple_copy(result, input);
  return false;
#endif
}



} // namespace detail


// String types
template <>
inline void
property::set<std::string>(char const * path,
    wyrd_merge_strategy _strategy, std::string const & _value,
    std::shared_ptr<handle> _handle /* = {} */)
{
  auto [err, strat] = get_strategy(_handle, _strategy);
  if (WYRD_ERR_SUCCESS != err) {
    throw wyrd::exception{err, "No strategy available!"};
  }

  auto new_val = std::make_shared<property_value>(WYRD_PT_UTF8_STRING);
  detail::convert_to_utf8(new_val->raw_memory(), _value);

  auto [err2, edit] = strat->apply_value(path, m_value, new_val, m_strategy, _strategy);
  if (WYRD_ERR_SUCCESS != err2) {
    throw wyrd::exception{err, "Unable to apply new property value!"};
  }

  m_strategy = _strategy;

  err = maybe_persist_edit(_handle, edit);
  if (WYRD_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Could not persist edit!");
  }
}


template <>
struct property_mapping<std::string>
{
  WYRD_PROPERTY_MAPPING_DEFAULTS

  static constexpr int type = WYRD_PT_UTF8_STRING;

  inline operator std::string() const
  {
    return std::string{prop.raw_value().begin(), prop.raw_value().end()};
  }


  inline bool operator==(std::string const & other) const
  {
    return static_cast<std::string>(*this) == other;
  }
};

inline bool operator==(std::string const & left, property_mapping<std::string> const & right)
{
  return right == left;
}


// raw_memory_type as a blob
template <>
inline void
property::set<property_value::raw_memory_type>(char const * path,
    wyrd_merge_strategy _strategy,
    property_value::raw_memory_type const & _value,
    std::shared_ptr<handle> _handle /* = {} */)
{
  auto [err, strat] = get_strategy(_handle, _strategy);
  if (WYRD_ERR_SUCCESS != err) {
    throw wyrd::exception{err, "No strategy available!"};
  }

  auto new_val = std::make_shared<property_value>(WYRD_PT_BLOB, _value.data(), _value.size());
  auto [err2, edit] = strat->apply_value(path, m_value, new_val, m_strategy, _strategy);

  if (WYRD_ERR_SUCCESS != err2) {
    throw wyrd::exception{err, "Unable to apply new property value!"};
  }

  m_strategy = _strategy;

  err = maybe_persist_edit(_handle, edit);
  if (WYRD_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Could not persist edit!");
  }
}



template <>
struct property_mapping<property_value::raw_memory_type>
{
  WYRD_PROPERTY_MAPPING_DEFAULTS

  static constexpr int type = WYRD_PT_BLOB;

  inline operator property_value::raw_memory_type const & () const
  {
    if (!prop.m_value) {
      throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Cannot dereference empty property!"};
    }
    return prop.m_value->raw_memory();
  }

  inline operator property_value::raw_memory_type & ()
  {
    if (!prop.m_value) {
      throw wyrd::exception{WYRD_ERR_BAD_PROPERTY_TYPE, "Cannot dereference empty property!"};
    }
    return prop.m_value->raw_memory();
  }


  inline bool operator==(property_value::raw_memory_type const & other) const
  {
    return static_cast<property_value::raw_memory_type>(*this) == other;
  }
};

inline bool operator==(property_value::raw_memory_type const & left, property_mapping<property_value::raw_memory_type> const & right)
{
  return right == left;
}

} // namespace wyrd


#endif // guard
