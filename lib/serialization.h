/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef WYRD_LIB_SERIALIZATION_H
#define WYRD_LIB_SERIALIZATION_H

#include <build-config.h>

#include <wyrd/properties.h>

#include <vector>

#include <liberate/serialization/varint.h>
#include <liberate/serialization/integer.h>

#include <liberate/logging.h>

#define WYRD_SERIALIZE_CASE(ptype, ctype) \
  case ptype: \
    LIBLOG_DEBUG("Serializing " # ptype); \
    { \
      auto ptr = static_cast<ctype const *>(static_cast<void const *>(value.data())); \
      used = liberate::serialization::serialize_int(offset, remaining, *ptr); \
      if (used != sizeof(ctype)) { \
        LIBLOG_ERROR("Unable to serialize value!"); \
        return 0; \
      } \
      offset += used; \
      remaining -= used; \
    } \
    break;


#define WYRD_SERIALIZE_CASE_AS(ptype, ctype, inttype) \
  case ptype: \
    LIBLOG_DEBUG("Serializing " # ptype); \
    { \
      auto ptr = static_cast<inttype const *>(static_cast<void const *>(value.data())); \
      used = liberate::serialization::serialize_int(offset, remaining, *ptr); \
      if (used != sizeof(ctype)) { \
        LIBLOG_ERROR("Unable to serialize value!"); \
        return 0; \
      } \
      offset += used; \
      remaining -= used; \
    } \
    break;


#define WYRD_DESERIALIZE_CASE(ptype, ctype) \
  case ptype: \
    LIBLOG_DEBUG("Deserializing " # ptype); \
    { \
      ctype val = 0; \
      used = liberate::serialization::deserialize_int(val, offset, remaining); \
      if (used != sizeof(ctype)) { \
        LIBLOG_ERROR("Unable to deserialize value!"); \
        return 0; \
      } \
      offset += used; \
      remaining -= used; \
      value.resize(used); \
      *static_cast<ctype *>(static_cast<void *>(value.data())) = val; \
    } \
    break;


#define WYRD_DESERIALIZE_CASE_AS(ptype, ctype, inttype) \
  case ptype: \
    LIBLOG_DEBUG("Deserializing " # ptype); \
    { \
      inttype val = 0; \
      used = liberate::serialization::deserialize_int(val, offset, remaining); \
      if (used != sizeof(ctype)) { \
        LIBLOG_ERROR("Unable to deserialize value!"); \
        return 0; \
      } \
      offset += used; \
      remaining -= used; \
      value.resize(used); \
      *static_cast<ctype *>(static_cast<void *>(value.data())) = *static_cast<ctype *>(static_cast<void *>(&val)); \
    } \
    break;




namespace wyrd::util {

/**
 * These are helper functions for serializing property_values. In all
 * likelihood, these will be re-used throughout merge strategies.
 */
inline size_t
serialize_value(char * buffer, size_t bufsize, wyrd_property_type type,
    std::vector<char> const & value)
{
  if (!buffer || !bufsize) {
    LIBLOG_ERROR("Bad parameter values.");
    return 0;
  }

  auto offset = buffer;
  auto remaining = bufsize;

  // Serializing an edit means serializing the value, which needs to start with
  // the value type.
  auto dummy = static_cast<liberate::types::varint>(type);
  auto used = liberate::serialization::serialize_varint(offset, remaining, dummy);
  if (!used) {
    LIBLOG_ERROR("Could not serialize value type field.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize the value size.
  dummy = static_cast<liberate::types::varint>(value.size());
  used = liberate::serialization::serialize_varint(offset, remaining, dummy);
  if (!used) {
    LIBLOG_ERROR("Could not serialize value size field.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // We need enough space for the value as well.
  if (remaining < value.size()) {
    LIBLOG_ERROR("Not enough space for value!");
    return 0;
  }

  // Next, we need to serialize the value. However, only strings and blobs can
  // be serialized as a sequence of bytes. For primitive types, we have to deal
  // with byte order.
  switch (type) {
    case WYRD_PT_BLOB:
    case WYRD_PT_UTF8_STRING:
      std::memcpy(offset, value.data(), value.size());
      offset += value.size();
      remaining -= value.size();
      break;

    WYRD_SERIALIZE_CASE(WYRD_PT_UINT8, uint8_t)
    WYRD_SERIALIZE_CASE(WYRD_PT_SINT8, int8_t)
    WYRD_SERIALIZE_CASE(WYRD_PT_UINT16, uint16_t)
    WYRD_SERIALIZE_CASE(WYRD_PT_SINT16, int16_t)
    WYRD_SERIALIZE_CASE(WYRD_PT_UINT32, uint32_t)
    WYRD_SERIALIZE_CASE(WYRD_PT_SINT32, int32_t)
    WYRD_SERIALIZE_CASE(WYRD_PT_UINT64, uint64_t)
    WYRD_SERIALIZE_CASE(WYRD_PT_SINT64, int64_t)

    WYRD_SERIALIZE_CASE_AS(WYRD_PT_BOOL, bool, uint8_t)
    WYRD_SERIALIZE_CASE_AS(WYRD_PT_FLOAT, float, uint32_t)
    WYRD_SERIALIZE_CASE_AS(WYRD_PT_DOUBLE, double, uint64_t)

    default:
      LIBLOG_ERROR("Cannot serialize unknown type!");
      return 0;
      break;
  }

  return (offset - buffer);
}



inline size_t
deserialize_value(wyrd_property_type & type, std::vector<char> & value,
    char const * buffer, size_t bufsize)
{
  if (!buffer || !bufsize) {
    LIBLOG_ERROR("Bad parameter values.");
    return 0;
  }

  auto offset = buffer;
  auto remaining = bufsize;

  // Deserialize the value type
  liberate::types::varint dummy;
  auto used = liberate::serialization::deserialize_varint(dummy, offset, remaining);
  if (!used) {
    LIBLOG_ERROR("Could not deserialize value type field.");
    return 0;
  }
  offset += used;
  remaining -= used;

  type = static_cast<wyrd_property_type>(dummy);

  // Deserialize the value size
  used = liberate::serialization::deserialize_varint(dummy, offset, remaining);
  if (!used) {
    LIBLOG_ERROR("Could not deserialize value size field.");
    return 0;
  }
  offset += used;
  remaining -= used;

  auto size = static_cast<size_t>(dummy);

  // We have to have enough buffer for the value
  if (remaining < size) {
    LIBLOG_ERROR("Not enough space for value!");
    return 0;
  }

  // Deserialize value
  switch (type) {
    case WYRD_PT_BLOB:
    case WYRD_PT_UTF8_STRING:
      value.resize(size);
      std::memcpy(value.data(), offset, size);
      offset += size;
      remaining -= size;
      break;

    WYRD_DESERIALIZE_CASE(WYRD_PT_UINT8, uint8_t)
    WYRD_DESERIALIZE_CASE(WYRD_PT_SINT8, int8_t)
    WYRD_DESERIALIZE_CASE(WYRD_PT_UINT16, uint16_t)
    WYRD_DESERIALIZE_CASE(WYRD_PT_SINT16, int16_t)
    WYRD_DESERIALIZE_CASE(WYRD_PT_UINT32, uint32_t)
    WYRD_DESERIALIZE_CASE(WYRD_PT_SINT32, int32_t)
    WYRD_DESERIALIZE_CASE(WYRD_PT_UINT64, uint64_t)
    WYRD_DESERIALIZE_CASE(WYRD_PT_SINT64, int64_t)

    WYRD_DESERIALIZE_CASE_AS(WYRD_PT_BOOL, bool, uint8_t)
    WYRD_DESERIALIZE_CASE_AS(WYRD_PT_FLOAT, float, uint32_t)
    WYRD_DESERIALIZE_CASE_AS(WYRD_PT_DOUBLE, double, uint64_t)

    default:
      LIBLOG_ERROR("Cannot deserialize unknown type!");
      return 0;
      break;
  }

  return (offset - buffer);
}



} // namespace wyrd::util

#endif // guard
