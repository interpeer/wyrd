==========================
Introduction to Properties
==========================

The core concept of wyrd are properties -- named and typed values, much like
variables in your favourite programming language. Wyrd provides properties for
a variety of integer and floating point types, as well as strings, lists and
key to value mappings.

If you read this and think that sounds just like `JSON`_, or perhaps if you're
older like `ASN.1`_ or a multitude of similar structured data serialization
formats, you're not entirely wrong. At its core, wyrd properties are not so
dissimilar.

The main difference is that these properties can have unique *merge strategies*,
the simplest of them being a "naive override". This is because unlike the above
serialization formats, properties are `conflict-free, replicated data types`_.
That is, wyrd doesn't serialize them as named values, but as edits of a previous
value (with the first edit being an edit from a null ancestor).

Consequently, serializing and deserializing properties is a little more complex.
But the benefit of doing so means that you can easily create distributed
applications. The serialization into edits means that edits from multiple
parties can be merged, whenever they are received e.g. from the network.

In the meantime, properties still function pretty much as you would expect them
to from such better known serialization formats.

.. _JSON: https://www.json.org
.. _ASN.1: https://www.itu.int/en/ITU-T/asn1/Pages/introduction.aspx
.. _conflict-free, replicated data types: https://crdt.tech

Structured Data
===============

If you have a number of properties in a single document, you end up with
structured data. Much like JSON or `YAML`_, the document is considered to have
a root element. Unlike both, the only root element you can use is a key-value
mapping.

Elements can be accessed via their *name* or *index*, and both can be arranged
into *paths*. The path element separator is the `.` (dot, full-stop) character.
The root element has no name (an empty name).

Imagine the following YAML document were in fact a representation of wyrd
properties:

.. sourcecode:: yaml
   :linenos:
   :dedent:
   :caption: Example Document

   foo: bar
   baz:
   - item1
   - key1: 42
     key2: 123

This means that the value at the path `.foo` is `bar` of type string. The value
at `.baz` is a list. `.baz.0` is just a string with value `item1`, while the
value at `.baz.1` is in fact another key-value mapping.

As you can see, this is not fundamentally different from how one might access
e.g. a value in a nested JSON structure in many programming languages.

.. _YAML: https://yaml.org/
