=============
How-To Guides
=============

This section of the documentation collects how-tos on various topics.

Properties
==========

:doc:`prop-monitor`
    Monitor properties for changes.

:doc:`vessel`
    Use wyrd with a vessel resource.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   prop-monitor
   vessel
