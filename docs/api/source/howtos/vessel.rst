==================
Vessel Integration
==================

The majority of this documentation focuses on how to work with wyrd documents
and properties. Wyrd is integrated with `vessel`_, however, for distributed
access to the same document.

.. _vessel: https://interpeer.io/projects/vessel/

Working with handles and properties does not change as a result of this
integration. What does change is how you obtain a handle. Rather than
opening a file, you instead need to open a vessel resource.

.. sourcecode:: c
   :linenos:
   :dedent:
   :caption: Create document handle from vessel resource

    #include <wyrd/handle.h>

    wyrd_error_t err = WYRD_ERR_SUCCESS;
    struct wyrd_handle * handle = NULL;

    err = wyrd_open_resource(api, &handle, "resource", WYRD_O_RW | WYRD_O_CREATE);
    assert(WYRD_ERR_SUCCESS == err);
    assert(handle != NULL);

This is enough for reading from the resource. In order to write properties,
however, vessel needs to know two things:

#. It needs to know an *author* key.
#. It needs to know the algorithms you choose when creating vessel extents.

This (and in future, potentially any other) information can be passed to
vessel by setting options on the opened resource handle.

.. sourcecode:: c
   :linenos:
   :dedent:
   :caption: Create algorithm choices and add them to handle

    vessel_algorithm_choices algos = vessel_algorithm_choices_create(
        "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1");
    wyrd_error_t err = wyrd_set_resource_option(handle, WYRD_RO_ALGORITHMS, &algos);

Setting the author works in much the same way. We assume a string `private_key`
that can be read by vessel below:

.. sourcecode:: c
   :linenos:
   :dedent:
   :caption: Create author and add it to handle

    vessel_error_t vessel_err = vessel_author_from_buffer(&author, &algos,
        private_key, strlen(private_key));

    err = wyrd_set_resource_option(handle, WYRD_RO_AUTHOR, author);

.. note:: The algorithm options are *copied* by the function, but the author
   structure, being opaque, is not. That means you need to ensure it lives for
   at least as long as the handle is open.

With these options set, you can now use the `handle` in exactly the same way as
other handles before.
