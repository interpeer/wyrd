=========
Copyright
=========

This documentation is provided by `Interpeer gUG`_, a tiny non-profit. It is
licensed under |license|_ which gives you a ton of rights, including the right
to share and adapt it.

If you find this documentation useful, you should consider `donating`_.

.. _Interpeer gUG: https://interpeer.io/
.. _donating: https://interpeer.io/donations/
