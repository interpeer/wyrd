================================
Welcome to Wyrd's documentation!
================================

This library implements a `conflict-free, replicated data type <https://crdt.tech>`_
(CRDT), but that is not the interesting part.

More broadly speaking, this library provides a means for treating documents
both as a *set of properties* (arranged in a tree-like fashion), as well as
*a sequence of edit operations*. This is what CRDTs are, right?

The interesting part is that this is not tied to any specific implementation
language. Yes, the library itself is implemented in C++, an yes, the library
itself provides a C API (for the broadest possible integration possibilities).
But just because you may use it in, say, JavaScript does not mean that the
property semantics are like you might expect from JSON.

Instead, the library acts as a language-neutral framework, and thus maximizes
interoperability between different programming languages when manipulating
structured data in parallel in distributed systems. Phew.

How data is synchronized is not *entirely* within the realm of this library's
concerns. It supports file I/O (on POSIX systems), but it can also use
`Vessel <https://interpeer.io/projects/vessel/>`_ as a container format. That
container format in turn is optimized to be synchronized by arbitrary means,
from Sneakernet to future networking protocols.

Using this Library
==================

:doc:`introduction`
    An introduction to conflict-free, replicated data types.

:doc:`getting-started`
    Getting started with the library.

:doc:`properties`
    An introduction to wyrd properties and their relationship to CRDTs.

:doc:`tutorials/index`
    Tutorials for starting development with wyrd.

:doc:`howtos/index`
    How-To Guides for using parts of this library.

:doc:`api/lib`
    Generated API documentation.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   self
   introduction
   getting-started
   properties
   tutorials/index
   howtos/index
   api/lib

.. todolist::
