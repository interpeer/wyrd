=================
Simple Properties
=================

In the :doc:`previous tutorial <create-doc>` we learned how to create a document
file. The next step is to work with properties. For this, we need a
`struct wyrd_handle *` instance just like we previously created. In this
tutorial, we'll just refer to it as the `handle` variable.

Set a Property
==============

Properties not only have a type, but also include a merge strategy. Which means
that you don't just set the value, but specify a merge strategy at the same time.

.. sourcecode:: c
   :linenos:
   :dedent:
   :caption: Set a property

    wyrd_error_t err = wyrd_set_property_uint16(handle, ".foo",
        WYRD_MS_NAIVE_OVERRIDE, 42);

You might have noticed that you're also setting a type, by invoking the function
with the `_uint16` postfix.

Internally, merge strategies are algorithms that take the previous property type
and value, and the newly given property type and value, and create a change
entry. The `WYRD_MS_NAIVE_OVERRIDE` strategy is very simply and just *sets* a
new value and type.

It's always possible to set a new value and type for the same property path.

.. sourcecode:: c
   :linenos:
   :dedent:
   :caption: Override a property

    err = wyrd_set_property_uint32(handle, ".foo",
        WYRD_MS_NAIVE_OVERRIDE, 1024 * 1024);

Setting strings or BLOBs requires providing a value pointer and data size.
Strings must be given as UTF-8 encoded.

.. sourcecode:: c
   :linenos:
   :dedent:
   :caption: Set a string property

    err = wyrd_set_property_utf8string(handle, ".bar",
        WYRD_MS_NAIVE_OVERRIDE, "Hello!", 6);

Note that the value is *copied* to the property; there is no need for you
to manage its memory lifetime.

Read a Property
===============

You can read a property using much the same as you can set it. However, there is
no need to provide a merge strategy.

.. sourcecode:: c
   :linenos:
   :dedent:
   :caption: Read a property

    uint16_t value = 0;
    err = wyrd_get_property_uint16(handle, ".foo", &value);

Observant readers will have noticed that we last set a `uint32_t`
property at the path `".foo"`. That means the above will *fail* with
a `WYRD_ERR_OUT_OF_MEMORY` error, as the size of the value type is less
than the size of the value stored.

Pass a `uint32_t`, and use the `_uint32` function, and it will succeed.

Much the same will occur when you pass to small of a buffer when reading
a BLOB or string property.

.. sourcecode:: c
   :linenos:
   :dedent:
   :caption: Read a string property

    char buf[200];
    size_t bufsize = sizeof(buf);
    err = wyrd_get_property_utf8string(handle, ".bar", buf, &bufsize);

After the call, the value of `bufsize` will specify the amount of buffer used
or required to read the value.

Query Properties
================

Of course, if you need to understand a property's type beforehand, you can
always query its current type.

.. sourcecode:: c
   :linenos:
   :dedent:
   :caption: Query a property

    wyrd_property_type type;

    err = wyrd_get_property_type(handle, ".foo", &type);
