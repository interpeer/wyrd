=========
Tutorials
=========

This section contains tutorials for getting you up and running with Wyrd
from scratch. It is recommended that you first read the
:doc:`../properties`.

Getting Started
===============

:doc:`create-doc`
    Create a wyrd document file.

:doc:`simple-props`
    Manipulate simple properties.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   create-doc
   simple-props
