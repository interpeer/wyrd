/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include "../../lib/merge_strategy.h"

#include "../../lib/merge_strategies/naive_override.h"

#include <liberate/string/hexencode.h>

namespace {

#ifdef WYRD_POSIX

struct context
{
  constexpr static char const * TEST_FILE = "testfile";

  std::shared_ptr<wyrd::handle> handle;

  inline context()
    : handle{std::make_shared<wyrd::posix_handle>(
        std::make_shared<wyrd::api>(),
        TEST_FILE,
        WYRD_O_RW | WYRD_O_CREATE)
      }
  {
  }
};

#endif // WYRD_POSIX


} // anonymous namespace


/*****************************************************************************
 * Naive override
 */

TEST(MergeStrategyNaiveOverride, serialize_edit)
{
#ifdef WYRD_POSIX
  // The property value to serialize
  uint16_t foo = 42;
  auto propval = std::make_shared<wyrd::property_value>(WYRD_PT_UINT16, &foo, sizeof(foo));

  // Create edit
  auto edit = std::make_shared<wyrd::naive_override_edit>(propval, "foo.bar");

  context ctx;

  // Use generic serialization function - this should use the appropriate merge
  // strategy.
  char buf[200];
  auto used = wyrd::merge_strategy::serialize_edit_generic(ctx.handle,
      buf, sizeof(buf), edit);

  // Ensure that at minimum 3+ bytes are used
  ASSERT_GT(used, 3);

  liberate::string::hexdump hd;
  std::cerr << hd(buf, used) << std::endl;

#else // WYRD_POSIX
  GTEST_SKIP() << "Not available on this platform";
#endif // WYRD_POSIX
}



TEST(MergeStrategyNaiveOverride, deserialize_edit)
{
#ifdef WYRD_POSIX
  char serialized[] = { '\x00', '\x00', '\x0d', '\x02', '\x04', 'w', 'y', 'r', 'd', '\x0c', '\x02', '\x00', '\x2a' };

  // Create empty edit
  std::shared_ptr<wyrd::edit> edit;

  context ctx;

  // Use generic function for deserializing edit
  auto [success, used] = wyrd::merge_strategy::deserialize_edit_generic(ctx.handle,
      edit, serialized, sizeof(serialized));

  // Ensure that at minimum 3+ bytes are used
  ASSERT_TRUE(success);
  ASSERT_GT(used, 3);

  // Ensure all the proper types
  ASSERT_TRUE(edit);
  ASSERT_EQ(WYRD_MS_NAIVE_OVERRIDE, edit->type);

  auto typed_edit = static_cast<wyrd::naive_override_edit *>(edit.get());
  ASSERT_TRUE(typed_edit->value);

  ASSERT_EQ(WYRD_PT_UINT16, typed_edit->value->type());
  ASSERT_EQ(2, typed_edit->value->raw_memory().size());
  auto val = *static_cast<uint16_t *>(static_cast<void *>(typed_edit->value->raw_memory().data()));

  ASSERT_EQ(42, val);

  ASSERT_EQ(edit->path, "wyrd");
#else // WYRD_POSIX
  GTEST_SKIP() << "Not available on this platform";
#endif // WYRD_POSIX
}
