/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include "../../lib/property_path.h"

namespace {

struct test_data
{
  std::string               input;
  std::vector<std::string>  parts;
} tests[] = {
  // Boundary
  { "", {} },
  { ".", {} },
  { "..", {} },

  // Simple dots
  { "foo", { "foo" } },
  { ".foo", { "foo" } },
  { "..foo", { "foo" } },
  { "foo.", { "foo" } },
  { "foo..", { "foo" } },
  { ".foo..", { "foo" } },
  { "..foo..", { "foo" } },

  // Dots in the middle
  { "foo.bar.baz", { "foo", "bar", "baz" } },
  { "foo..bar.baz", { "foo", "bar", "baz" } },
  { "foo.bar..baz", { "foo", "bar", "baz" } },
  { "foo...bar...baz", { "foo", "bar", "baz" } },

  // Complex
  { ".foo..bar.baz.z.quux..asdf",
    { "foo", "bar", "baz", "z", "quux", "asdf" } },
};



inline std::size_t
replace_all(std::string& inout, std::string_view what, std::string_view with)
{
    std::size_t count{};
    for (std::string::size_type pos{};
         inout.npos != (pos = inout.find(what.data(), pos, what.length()));
         pos += with.length(), ++count)
        inout.replace(pos, what.length(), with.data(), with.length());
    return count;
}


template <typename T>
std::string generate_name(testing::TestParamInfo<T> const & info)
{
  std::string name = info.param.input;
  replace_all(name, ".", "_dot_");
  if (name.empty()) {
    name = "_empty_";
  }
  return name;
}


inline void
test_path_iteration(std::string const & input, std::vector<std::string> const & parts)
{
  wyrd::property_path_segment seg;
  size_t count = 0;

  wyrd::path_segment_iterate(&seg, input.c_str(), input.size());
  while (count < parts.size()) {
    ASSERT_FALSE(seg.empty());
    ASSERT_TRUE(seg);
    ASSERT_EQ(seg.to_str(), parts[count]);

    ++count;
    wyrd::path_segment_iterate(&seg, input.c_str(), input.size());
  }

  ASSERT_EQ(count, parts.size());
}

} // anonymous namespace


class PropertyPath
  : public testing::TestWithParam<test_data>
{
};



TEST_P(PropertyPath, iteration)
{
  auto td = GetParam();
  test_path_iteration(td.input, td.parts);
}



INSTANTIATE_TEST_SUITE_P(props, PropertyPath, testing::ValuesIn(tests),
    generate_name<test_data>);
