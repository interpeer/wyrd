/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include "../../lib/properties.h"

namespace {

struct bad_type {};

} // anonymous namespace


template <typename T>
class PrimitiveProperty : public testing::Test
{
};

typedef testing::Types<
  int8_t,
  uint8_t,
  int16_t,
  uint16_t,
  int32_t,
  uint32_t,
  int64_t,
  uint64_t,
  float,
  double
> test_types;

TYPED_TEST_SUITE(PrimitiveProperty, test_types);


TYPED_TEST(PrimitiveProperty, construct_and_access)
{
  TypeParam foo = 42;
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, foo};

  ASSERT_EQ(42, prop.as<TypeParam>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);

  wyrd::property const & prop2{prop};
  ASSERT_EQ(42, prop2.as<TypeParam>());
  ASSERT_THROW(prop2.as<bad_type>(), wyrd::exception);
}


TYPED_TEST(PrimitiveProperty, construct_set_and_access)
{
  TypeParam foo = 42;
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, foo};

  ASSERT_EQ(42, prop.as<TypeParam>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);

  prop.set("", WYRD_MS_NAIVE_OVERRIDE, TypeParam{123});

  ASSERT_EQ(123, prop.as<TypeParam>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);
}




TEST(Property, construct_and_access_bool)
{
  bool foo = true;
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, foo};

  ASSERT_TRUE(prop.as<bool>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);

  wyrd::property const & prop2{prop};
  ASSERT_TRUE(prop2.as<bool>());
  ASSERT_THROW(prop2.as<bad_type>(), wyrd::exception);
}


TEST(Property, construct_set_and_access_bool)
{
  bool foo = true;
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, foo};

  ASSERT_TRUE(prop.as<bool>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);

  prop.set("", WYRD_MS_NAIVE_OVERRIDE, false);
  ASSERT_FALSE(prop.as<bool>());
}



TEST(Property, construct_and_access_string)
{
  std::string foo = "Hello, world!";
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, foo};

  ASSERT_EQ("Hello, world!", prop.as<std::string>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);

  wyrd::property const & prop2{prop};
  ASSERT_EQ("Hello, world!", prop2.as<std::string>());
  ASSERT_THROW(prop2.as<bad_type>(), wyrd::exception);
}


TEST(Property, construct_set_and_access_string)
{
  std::string foo = "Hello, world!";
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, foo};

  ASSERT_EQ("Hello, world!", prop.as<std::string>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);

  prop.set("", WYRD_MS_NAIVE_OVERRIDE, std::string{"Goodbye, world!"});
  ASSERT_EQ("Goodbye, world!", prop.as<std::string>());
}



TEST(Property, construct_and_access_string_utf8)
{
  using namespace std::string_literals;

  std::string foo = u8"Hello, 🤣😀 world!";
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, foo};

  ASSERT_EQ(u8"Hello, 🤣😀 world!", prop.as<std::string>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);

  wyrd::property const & prop2{prop};
  ASSERT_EQ(u8"Hello, 🤣😀 world!", prop2.as<std::string>());
  ASSERT_THROW(prop2.as<bad_type>(), wyrd::exception);
}


TEST(Property, construct_and_access_string_utf16)
{
  using namespace std::string_literals;

  char data[] = {
    '\x00', '\x48', '\x00', '\x65', '\x00', '\x6c', '\x00', '\x6c', '\x00',
    '\x6f', '\x00', '\x2c', '\x00', '\x20', '\xd8', '\x3e', '\xdd', '\x23',
    '\xd8', '\x3d', '\xde', '\x00', '\x00', '\x20', '\x00', '\x77', '\x00',
    '\x6f', '\x00', '\x72', '\x00', '\x6c', '\x00', '\x64', '\x00', '\x21',
  };

  auto foo = std::string{data, data + sizeof(data)};
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, foo};

  ASSERT_EQ(u8"Hello, 🤣😀 world!", prop.as<std::string>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);

  wyrd::property const & prop2{prop};
  ASSERT_EQ(u8"Hello, 🤣😀 world!", prop2.as<std::string>());
  ASSERT_THROW(prop2.as<bad_type>(), wyrd::exception);
}


TEST(Property, construct_and_access_blob)
{
  std::string tmp = "Hello, world!";
  std::vector<char> foo{tmp.begin(), tmp.end()};
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, foo};

  ASSERT_EQ(foo, prop.as<std::vector<char>>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);

  wyrd::property const & prop2{prop};
  ASSERT_EQ(foo, prop2.as<std::vector<char>>());
  ASSERT_THROW(prop2.as<bad_type>(), wyrd::exception);
}


TEST(Property, construct_set_and_access_blob)
{
  std::string tmp = "Hello, world!";
  std::vector<char> foo{tmp.begin(), tmp.end()};
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, foo};

  ASSERT_EQ(foo, prop.as<std::vector<char>>());
  ASSERT_THROW(prop.as<bad_type>(), wyrd::exception);

  tmp = "Goodbye, world!";
  std::vector<char> bar{tmp.begin(), tmp.end()};
  prop.set("", WYRD_MS_NAIVE_OVERRIDE, bar);

  ASSERT_EQ(bar, prop.as<std::vector<char>>());
}


TEST(Property, copy_construct)
{
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, uint16_t{42}};
  ASSERT_EQ(WYRD_PT_UINT16, prop.type());

  auto prop2{prop};
  ASSERT_NE(&prop, &prop2); // Not a reference

  ASSERT_EQ(WYRD_PT_UINT16, prop2.type());
  ASSERT_EQ(WYRD_MS_NAIVE_OVERRIDE, prop2.strategy());
  ASSERT_EQ(uint16_t{42}, prop2.as<uint16_t>());
}


TEST(Property, assign)
{
  wyrd::property prop{WYRD_MS_NAIVE_OVERRIDE, uint16_t{42}};
  ASSERT_EQ(WYRD_PT_UINT16, prop.type());

  wyrd::property prop2{WYRD_MS_NAIVE_OVERRIDE, std::string{"Hello, world!"}};
  ASSERT_EQ(WYRD_PT_UTF8_STRING, prop2.type());

  prop2 = prop;

  ASSERT_EQ(WYRD_PT_UINT16, prop.type());
  ASSERT_EQ(WYRD_PT_UINT16, prop2.type());

  ASSERT_EQ(uint16_t{42}, prop);
  ASSERT_EQ(uint16_t{42}, prop2);
}


TEST(Property, list_access_read)
{
  wyrd::property prop{WYRD_PT_CONT_LIST};
  ASSERT_EQ(WYRD_PT_CONT_LIST, prop.type());

  prop.append(WYRD_MS_NAIVE_OVERRIDE, 42);
  prop.append(WYRD_MS_NAIVE_OVERRIDE, std::string{"Hello!"});

  ASSERT_EQ(42, prop[0]);
  ASSERT_EQ(std::string{"Hello!"}, prop[1]);

  ASSERT_THROW(prop[0].as<std::string>(), wyrd::exception);
  ASSERT_THROW(prop[1].as<int>(), wyrd::exception);
}


TEST(Property, list_access_write)
{
  wyrd::property prop{WYRD_PT_CONT_LIST};
  ASSERT_EQ(WYRD_PT_CONT_LIST, prop.type());

  prop.append(WYRD_MS_NAIVE_OVERRIDE, 42);
  prop.append(WYRD_MS_NAIVE_OVERRIDE, std::string{"Hello!"});

  prop[0] = wyrd::property{WYRD_MS_NAIVE_OVERRIDE, std::string{"World :)"}};

  ASSERT_EQ(std::string{"World :)"}, prop[0]);
  ASSERT_EQ(std::string{"Hello!"}, prop[1]);
}


TEST(Property, map_access_read)
{
  wyrd::property prop{WYRD_PT_CONT_MAP};
  ASSERT_EQ(WYRD_PT_CONT_MAP, prop.type());

  prop.insert("foo", WYRD_MS_NAIVE_OVERRIDE, 42);
  prop.insert("bar", WYRD_MS_NAIVE_OVERRIDE, std::string{"Hello!"});

  ASSERT_EQ(42, prop["foo"]);
  ASSERT_EQ(std::string{"Hello!"}, prop["bar"]);
}


TEST(Property, map_access_write)
{
  wyrd::property prop{WYRD_PT_CONT_MAP};
  ASSERT_EQ(WYRD_PT_CONT_MAP, prop.type());

  prop.insert("foo", WYRD_MS_NAIVE_OVERRIDE, 42);
  prop.insert("bar", WYRD_MS_NAIVE_OVERRIDE, std::string{"Hello!"});

  prop["foo"] = wyrd::property{WYRD_MS_NAIVE_OVERRIDE, std::string{"World :)"}};

  ASSERT_EQ(std::string{"World :)"}, prop["foo"]);
  ASSERT_EQ(std::string{"Hello!"}, prop["bar"]);
}


TEST(Property, map_access_write_by_operator)
{
  wyrd::property prop{WYRD_PT_CONT_MAP};
  ASSERT_EQ(WYRD_PT_CONT_MAP, prop.type());

  // Create nested structures in an explicitly created map
  prop["foo"] = wyrd::property{WYRD_PT_CONT_MAP};
  prop["foo"]["bar"] = wyrd::property{WYRD_MS_NAIVE_OVERRIDE, std::string{"World :)"}};
  prop["foo"]["baz"] = wyrd::property{WYRD_MS_NAIVE_OVERRIDE, uint8_t{42}};

  ASSERT_EQ(WYRD_PT_CONT_MAP, prop["foo"].type());
  ASSERT_EQ(WYRD_PT_UTF8_STRING, prop["foo"]["bar"].type());
  ASSERT_EQ(WYRD_PT_UINT8, prop["foo"]["baz"].type());

  // Now in an implicit map
  prop["quux"]["foo"] = wyrd::property{WYRD_MS_NAIVE_OVERRIDE, uint8_t{42}};
  ASSERT_EQ(WYRD_PT_UINT8, prop["quux"]["foo"].type());
}



TEST(Property, map_path_access_write)
{
  wyrd::property prop{WYRD_PT_CONT_MAP};
  ASSERT_EQ(WYRD_PT_CONT_MAP, prop.type());

  prop.insert("foo.bar.baz", WYRD_MS_NAIVE_OVERRIDE, 42);

  ASSERT_EQ(WYRD_PT_CONT_MAP, prop.type());
  ASSERT_EQ(WYRD_PT_CONT_MAP, prop["foo"].type());
  ASSERT_EQ(WYRD_PT_CONT_MAP, prop["foo"]["bar"].type());

  ASSERT_EQ(42, prop["foo"]["bar"]["baz"]);
  ASSERT_EQ(42, prop[".foo.bar.baz"]);

  wyrd::property const & prop2{prop};
  ASSERT_EQ(42, prop2["foo"]["bar"]["baz"]);
  ASSERT_EQ(42, prop2[".foo.bar.baz"]);

  ASSERT_THROW(prop2["foo"]["quux"], wyrd::exception);
  ASSERT_THROW(prop2["foo.quux"], wyrd::exception);
}


TEST(Property, mixed_path_access)
{
  wyrd::property prop{WYRD_PT_CONT_MAP};
  ASSERT_EQ(WYRD_PT_CONT_MAP, prop.type());

  prop["foo"] = wyrd::property{WYRD_PT_CONT_LIST};
  prop["foo"].append(WYRD_MS_NAIVE_OVERRIDE, 42);
  prop["foo"].append(WYRD_MS_NAIVE_OVERRIDE, std::string{"test"});
  prop["foo"].append(wyrd::property{WYRD_PT_CONT_MAP});

  // Operator based access
  ASSERT_EQ(42, prop["foo"][0]);
  ASSERT_EQ(std::string{"test"}, prop["foo"][1]);

  // Path based access
  ASSERT_EQ(42, prop["foo.0"]);
  ASSERT_EQ(std::string{"test"}, prop[".foo.1"]);

  // Accessing the nested list should also work.
  prop[".foo.2.bar"] = wyrd::property{WYRD_MS_NAIVE_OVERRIDE, 42};
  ASSERT_EQ(42, prop["foo"][2]["bar"]);

  // However, you can't just add new indices to an array; that will throw.
  ASSERT_THROW(prop[".foo.17"], wyrd::exception);
  ASSERT_THROW(prop[".foo.3"], wyrd::exception);
}





TEST(Property, unknown_type)
{
  wyrd::property prop{WYRD_PT_UNKNOWN};
  ASSERT_EQ(WYRD_PT_UNKNOWN, prop.type());
}
