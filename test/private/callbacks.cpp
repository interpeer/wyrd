/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include "../../lib/callbacks.h"

namespace {

void test_callback(struct wyrd_handle *,
    char const *, wyrd_property_type,
    void const *, size_t, void *)
{
  // Do nothing.
}

} // anonymous namespace



TEST(PropertyCallbacks, set_uniqueness_ignores_refcount)
{
  wyrd::property_callback_set set;

  int baton = 42;

  // First insert should be new element
  {
    auto [iter, new_element] = set.insert(wyrd::callback_data{test_callback, &baton});
    ASSERT_TRUE(new_element);
    ASSERT_EQ(0, iter->refcount);
    ++(iter->refcount);
  }

  // Second insert should be the previous element
  {
    auto [iter, new_element] = set.insert(wyrd::callback_data{test_callback, &baton});
    ASSERT_FALSE(new_element);
    ASSERT_EQ(1, iter->refcount);
  }
}


TEST(PropertyCallbacks, set_uniqueness)
{
  wyrd::property_callback_set set;

  int baton1 = 42;
  int baton2 = 43;

  // First insert should be new element
  {
    auto [iter, new_element] = set.insert(wyrd::callback_data{test_callback, &baton1});
    ASSERT_TRUE(new_element);
    ASSERT_EQ(0, iter->refcount);
    ++(iter->refcount);
  }

  // Insert with a different baton should also work.
  {
    auto [iter, new_element] = set.insert(wyrd::callback_data{test_callback, &baton2});
    ASSERT_TRUE(new_element);
    ASSERT_EQ(0, iter->refcount);
    ++(iter->refcount);
  }
}
