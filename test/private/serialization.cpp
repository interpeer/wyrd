/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include "../../lib/serialization.h"

#include <liberate/string/hexencode.h>
#include <liberate/sys/memory.h>

namespace {

template <wyrd_property_type PTYPE, typename CTYPE>
struct holder
{
  static constexpr wyrd_property_type property_type = PTYPE;
  using type = CTYPE;
};


inline void
test_serialization_as(wyrd_property_type type)
{
  // Create value buffer
  char val[] = "Hello, world";
  std::vector<char> buf{val, val + sizeof(val)};
  ASSERT_EQ(buf.size(), sizeof(val));

  liberate::string::hexdump hd;
  std::cerr << hd(buf.data(), buf.size()) << std::endl;

  // Serialize as type
  char tmp[200];

  auto used = wyrd::util::serialize_value(tmp, sizeof(tmp), type,
      buf);
  ASSERT_GT(used, buf.size());

  // Deserialize
  liberate::sys::secure_memzero(buf.data(), buf.size());
  buf.clear();
  wyrd_property_type dtype = WYRD_PT_UNKNOWN;

  auto used2 = wyrd::util::deserialize_value(dtype, buf, tmp, sizeof(tmp));
  ASSERT_EQ(used, used2);
  ASSERT_EQ(type, type);
  ASSERT_EQ(sizeof(val), buf.size());
  ASSERT_EQ(val, std::string{buf.data()});
}


} // anonymous namespace

template <typename T>
class UtilSerialization : public testing::Test
{
};

typedef testing::Types<
  holder<WYRD_PT_SINT8, int8_t>,
  holder<WYRD_PT_UINT8, uint8_t>,
  holder<WYRD_PT_SINT16, int16_t>,
  holder<WYRD_PT_UINT16, uint16_t>,
  holder<WYRD_PT_SINT32, int32_t>,
  holder<WYRD_PT_UINT32, uint32_t>,
  holder<WYRD_PT_SINT64, int64_t>,
  holder<WYRD_PT_UINT64, uint64_t>,
  holder<WYRD_PT_BOOL, bool>,
  holder<WYRD_PT_FLOAT, float>,
  holder<WYRD_PT_DOUBLE, double>
> test_types;

TYPED_TEST_SUITE(UtilSerialization, test_types);

TYPED_TEST(UtilSerialization, serialize_and_deserialize)
{
  // Create value buffer
  typename TypeParam::type val = 42;
  std::vector<char> buf;
  buf.resize(sizeof(val));
  *buf.data() = val;

  liberate::string::hexdump hd;
  std::cerr << hd(buf.data(), buf.size()) << std::endl;

  // Serialize as type
  char tmp[200];

  auto used = wyrd::util::serialize_value(tmp, sizeof(tmp), TypeParam::property_type,
      buf);
  ASSERT_GT(used, buf.size());

  // Deserialize
  liberate::sys::secure_memzero(buf.data(), buf.size());
  ASSERT_NE(*buf.data(), val);
  buf.clear();
  wyrd_property_type type = WYRD_PT_UNKNOWN;

  auto used2 = wyrd::util::deserialize_value(type, buf, tmp, sizeof(tmp));
  ASSERT_EQ(used, used2);
  ASSERT_EQ(type, TypeParam::property_type);
  ASSERT_EQ(sizeof(val), buf.size());
  ASSERT_EQ(*buf.data(), val);
}



TEST(UtilSerialization, serialize_and_deserialize_blob)
{
  test_serialization_as(WYRD_PT_BLOB);
}



TEST(UtilSerialization, serialize_and_deserialize_utf8string)
{
  test_serialization_as(WYRD_PT_UTF8_STRING);
}
