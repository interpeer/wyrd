/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include "../../lib/handles/posix_handle.h"

#include <cstring>

static auto TEST_FILENAME = "wyrd-posix-handle-test";

TEST(PosixHandle, write_commit_read)
{
  auto api = std::make_shared<wyrd::api>();
  std::string test = "Hello, world!";

  // Write
  {
    wyrd::posix_handle handle{api, TEST_FILENAME, WYRD_O_WRITE | WYRD_O_CREATE};

    std::unique_ptr<wyrd::handle::range> range;

    ASSERT_FALSE(range);
    ASSERT_EQ(WYRD_ERR_SUCCESS, handle.allocate(range, test.size()));
    ASSERT_TRUE(range);

    ASSERT_EQ(range->size(), test.size());
    ASSERT_TRUE(range->buffer());

    std::memcpy(range->buffer(), test.c_str(), test.size());

    ASSERT_EQ(WYRD_ERR_SUCCESS, range->commit());
  }

  // Read
  {
    wyrd::posix_handle handle{api, TEST_FILENAME, WYRD_O_READ};

    std::vector<char> buf;
    buf.resize(test.size() * 2);
    size_t bufsize = buf.size();
    ASSERT_EQ(WYRD_ERR_SUCCESS, handle.read(&buf[0], bufsize));
    ASSERT_EQ(bufsize, test.size());

    std::string tmp{buf.begin(), buf.begin() + bufsize};
    ASSERT_EQ(tmp, test);
  }

  ::unlink(TEST_FILENAME);
}


TEST(PosixHandle, write_discard_explicitly_read)
{
  auto api = std::make_shared<wyrd::api>();
  std::string test = "Hello, world!";

  // Write
  {
    wyrd::posix_handle handle{api, TEST_FILENAME, WYRD_O_WRITE | WYRD_O_CREATE};

    std::unique_ptr<wyrd::handle::range> range;

    ASSERT_FALSE(range);
    ASSERT_EQ(WYRD_ERR_SUCCESS, handle.allocate(range, test.size()));
    ASSERT_TRUE(range);

    ASSERT_EQ(range->size(), test.size());
    ASSERT_TRUE(range->buffer());

    std::memcpy(range->buffer(), test.c_str(), test.size());

    ASSERT_EQ(WYRD_ERR_SUCCESS, range->discard());
  }

  // Read
  {
    wyrd::posix_handle handle{api, TEST_FILENAME, WYRD_O_READ};

    std::vector<char> buf;
    buf.resize(test.size() * 2);
    size_t bufsize = buf.size();
    ASSERT_EQ(WYRD_ERR_SUCCESS, handle.read(&buf[0], bufsize));
    ASSERT_EQ(bufsize, 0);
  }

  ::unlink(TEST_FILENAME);
}


TEST(PosixHandle, write_discard_implicitly_read)
{
  auto api = std::make_shared<wyrd::api>();
  std::string test = "Hello, world!";

  // Write
  {
    wyrd::posix_handle handle{api, TEST_FILENAME, WYRD_O_WRITE | WYRD_O_CREATE};

    std::unique_ptr<wyrd::handle::range> range;

    ASSERT_FALSE(range);
    ASSERT_EQ(WYRD_ERR_SUCCESS, handle.allocate(range, test.size()));
    ASSERT_TRUE(range);

    ASSERT_EQ(range->size(), test.size());
    ASSERT_TRUE(range->buffer());

    std::memcpy(range->buffer(), test.c_str(), test.size());

    // XXX just let range go out of scope
    // ASSERT_EQ(WYRD_ERR_SUCCESS, range->discard());
  }

  // Read
  {
    wyrd::posix_handle handle{api, TEST_FILENAME, WYRD_O_READ};

    std::vector<char> buf;
    buf.resize(test.size() * 2);
    size_t bufsize = buf.size();
    ASSERT_EQ(WYRD_ERR_SUCCESS, handle.read(&buf[0], bufsize));
    ASSERT_EQ(bufsize, 0);
  }

  ::unlink(TEST_FILENAME);
}
