/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <wyrd/api.h>
#include <wyrd/handle.h>

TEST(API, create_and_destroy)
{
  // Empty arguments don't work
  ASSERT_EQ(WYRD_ERR_INVALID_VALUE, wyrd_create(nullptr));
  ASSERT_EQ(WYRD_ERR_INVALID_VALUE, wyrd_destroy(nullptr));

  // Create an API
  struct wyrd_api * api = nullptr;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_create(&api));
  ASSERT_NE(api, nullptr);

  // Re-create it
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_create(&api));
  ASSERT_NE(api, nullptr);

  // Destroy it
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_destroy(&api));
  ASSERT_EQ(api, nullptr);
}


TEST(Handle, create_and_destroy_file)
{
  struct wyrd_api * api = nullptr;
  EXPECT_EQ(WYRD_ERR_SUCCESS, wyrd_create(&api));

  // Open
  struct wyrd_handle * handle = nullptr;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_open_file(api, &handle, "testfile", WYRD_O_RW | WYRD_O_CREATE));

  // Re-open
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_open_file(api, &handle, "testfile", WYRD_O_READ));

  // Close
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_close(&handle));

  EXPECT_EQ(WYRD_ERR_SUCCESS, wyrd_destroy(&api));
}
