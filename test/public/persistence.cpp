/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <wyrd/properties.h>
#include <vessel/author.h>

#define TAG_LINE \
  std::cerr << "=== " << __LINE__ << " ================================" << std::endl;

namespace {

struct posix_context
{
  constexpr static char const * TEST_FILE = "persistence-testfile";

  struct wyrd_api *     api = nullptr;
  struct wyrd_handle *  handle = nullptr;
  bool                  do_delete = false;
  char const *          filename = nullptr;

  inline posix_context(bool _do_delete, char const * _filename = TEST_FILE,
      int extra_flags = 0)
    : do_delete{_do_delete}
    , filename{_filename}
  {
    auto err = wyrd_create(&api);
    if (WYRD_ERR_SUCCESS != err) {
      std::cerr << "ERROR [" << __LINE__ << "]: " << wyrd_error_name(err) << " // " << wyrd_error_message(err) << std::endl;
      throw std::runtime_error{"TEST: Unable to create API"};
    }
    err = wyrd_open_file(api, &handle, filename, WYRD_O_RW | WYRD_O_CREATE | extra_flags);
    if (WYRD_ERR_SUCCESS != err) {
      std::cerr << "ERROR [" << __LINE__ << "]: " << wyrd_error_name(err) << " // " << wyrd_error_message(err) << std::endl;
      throw std::runtime_error{"TEST: Unable to create handle"};
    }
  }

  inline ~posix_context()
  {
    wyrd_close(&handle);
    wyrd_destroy(&api);
    if (do_delete) {
      std::cerr << "Test posix_context deleting " << filename << std::endl;
      ::unlink(filename);
    }
  }
};



struct vessel_context
{
  static constexpr auto ed_private_key = R"(-----BEGIN PRIVATE KEY-----
MC4CAQAwBQYDK2VwBCIEIE6V3BprgqVmv18GD75hBkxAUMLZM8KTy7JntEA1abN6
-----END PRIVATE KEY-----
  )";

  static constexpr auto ed_public_key = R"(-----BEGIN PUBLIC KEY-----
MCowBQYDK2VwAyEA/LHBym63DYPoVAUWl1QWJWprDP7QWPDZEdUPVsb2ynw=
-----END PUBLIC KEY-----
  )";

  constexpr static char const * TEST_FILE = "persistence-testfile";

  struct wyrd_api *     api = nullptr;
  struct wyrd_handle *  handle = nullptr;

  struct vessel_algorithm_choices algo_choices{};
  struct vessel_author *  author = nullptr;

  bool                  do_delete = false;
  char const *          filename = nullptr;

  inline vessel_context(bool _do_delete, char const * _filename = TEST_FILE,
      int extra_flags = 0)
    : do_delete{_do_delete}
    , filename{_filename}
  {
    auto err = wyrd_create(&api);
    if (WYRD_ERR_SUCCESS != err) {
      std::cerr << "ERROR [" << __LINE__ << "]: " << wyrd_error_name(err) << " // " << wyrd_error_message(err) << std::endl;
      throw std::runtime_error{"TEST: Unable to create API"};
    }
    err = wyrd_open_resource(api, &handle, filename, WYRD_O_RW | WYRD_O_CREATE | extra_flags);
    if (WYRD_ERR_SUCCESS != err) {
      std::cerr << "ERROR [" << __LINE__ << "]: " << wyrd_error_name(err) << " // " << wyrd_error_message(err) << std::endl;
      throw std::runtime_error{"TEST: Unable to create handle"};
    }

    algo_choices = vessel_algorithm_choices_create("sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1");
    auto vessel_err = vessel_author_from_buffer(&author, &algo_choices,
        ed_private_key, strlen(ed_private_key));
    if (VESSEL_ERR_SUCCESS != vessel_err) {
      std::cerr << "ERROR [" << __LINE__ << "]: " << vessel_error_name(err) << " // " << vessel_error_message(err) << std::endl;
      throw std::runtime_error{"TEST: Unable to create handle"};
    }

    err = wyrd_set_resource_option(handle, WYRD_RO_AUTHOR, author);
    if (WYRD_ERR_SUCCESS != err) {
      std::cerr << "ERROR [" << __LINE__ << "]: " << wyrd_error_name(err) << " // " << wyrd_error_message(err) << std::endl;
      throw std::runtime_error{"TEST: Unable to set author option"};
    }

    err = wyrd_set_resource_option(handle, WYRD_RO_ALGORITHMS, &algo_choices);
    if (WYRD_ERR_SUCCESS != err) {
      std::cerr << "ERROR [" << __LINE__ << "]: " << wyrd_error_name(err) << " // " << wyrd_error_message(err) << std::endl;
      throw std::runtime_error{"TEST: Unable to set algorithm option"};
    }
  }

  inline ~vessel_context()
  {
    wyrd_close(&handle);
    wyrd_destroy(&api);

    vessel_author_free(&author);

    if (do_delete) {
      std::cerr << "Test vessel_context deleting " << filename << std::endl;
      ::unlink(filename);
    }
  }
};


} // anonymous namespace


TEST(Persistence, single_edit)
{
#ifdef WYRD_POSIX

  // Persistence means that a handle (via posix_context) should contain the properties
  // written into them.
  // TAG_LINE
  {
    posix_context ctx{false};

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 42, 1));
  }
  // TAG_LINE
  {
    // FIXME posix_context ctx{true}; // unlink after use
    posix_context ctx{true};

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "foo.bar",
          WYRD_PT_UINT16));
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE));

    uint16_t val = 0;
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_uint16(ctx.handle, "foo.bar",
          &val));
    ASSERT_EQ(42, val);
  }
  // TAG_LINE

#else // WYRD_POSIX
  GTEST_SKIP() << "File access not implemented for non-POSIX systems.";
#endif // WYRD_POSIX
}


TEST(Persistence, multi_edit_preserves_last)
{
#ifdef WYRD_POSIX

  // Persistence means that a handle (via posix_context) should contain the properties
  // written into them.
  // TAG_LINE
  {
    posix_context ctx{false};

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 42, 1));

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 0xacab, 1));

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 1312, 1));
  }
  // TAG_LINE
  {
    // FIXME posix_context ctx{true}; // unlink after use
    posix_context ctx{true};

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "foo.bar",
          WYRD_PT_UINT16));
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE));

    uint16_t val = 0;
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_uint16(ctx.handle, "foo.bar",
          &val));
    ASSERT_EQ(1312, val);
  }
  // TAG_LINE

#else // WYRD_POSIX
  GTEST_SKIP() << "File access not implemented for non-POSIX systems.";
#endif // WYRD_POSIX
}


TEST(Persistence, multi_edit_change_type)
{
#ifdef WYRD_POSIX

  // Persistence means that a handle (via posix_context) should contain the properties
  // written into them.
  // TAG_LINE
  {
    posix_context ctx{false};

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 42, 1));

    char str[] = "The quick brown fox jumped over the lazy dog";
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_utf8string(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, str, sizeof(str), 1));

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 1312, 1));
  }
  // TAG_LINE
  {
    // FIXME posix_context ctx{true}; // unlink after use
    posix_context ctx{true};

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "foo.bar",
          WYRD_PT_UINT16));
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE));

    uint16_t val = 0;
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_uint16(ctx.handle, "foo.bar",
          &val));
    ASSERT_EQ(1312, val);
  }
  // TAG_LINE

#else // WYRD_POSIX
  GTEST_SKIP() << "File access not implemented for non-POSIX systems.";
#endif // WYRD_POSIX
}


TEST(Persistence, multi_edit_multi_value)
{
#ifdef WYRD_POSIX
  char str[] = "The quick brown fox jumped over the lazy dog";

  // Persistence means that a handle (via posix_context) should contain the properties
  // written into them.
  // TAG_LINE
  {
    posix_context ctx{false};

    // foo.bar
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 42, 1));

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_utf8string(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, str, sizeof(str), 1));

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 1312, 1));

    // foo.baz
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.baz",
          WYRD_MS_NAIVE_OVERRIDE, 42, 1));

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_utf8string(ctx.handle, "foo.baz",
          WYRD_MS_NAIVE_OVERRIDE, str, sizeof(str), 1));

    // quux
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_utf8string(ctx.handle, "quux",
          WYRD_MS_NAIVE_OVERRIDE, str, sizeof(str), 1));

    // foo.bar again
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 42, 1));

  }
  // TAG_LINE
  {
    // FIXME posix_context ctx{true}; // unlink after use
    posix_context ctx{false};

    // foo.bar
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "foo.bar",
          WYRD_PT_UINT16));
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE));

    uint16_t val = 0;
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_uint16(ctx.handle, "foo.bar",
          &val));
    ASSERT_EQ(42, val);

    // foo.baz
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "foo.baz",
          WYRD_PT_UTF8_STRING));
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "foo.baz",
          WYRD_MS_NAIVE_OVERRIDE));

    char buf[500];
    size_t used = sizeof(buf);
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_utf8string(ctx.handle, "foo.baz",
          buf, &used));
    ASSERT_EQ(used, sizeof(str));
    ASSERT_EQ(0, memcmp(str, buf, used));

    // quux
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "quux",
          WYRD_PT_UTF8_STRING));
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "quux",
          WYRD_MS_NAIVE_OVERRIDE));

    buf[0] = '\0';
    used = sizeof(buf);
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_utf8string(ctx.handle, "quux",
          buf, &used));
    ASSERT_EQ(used, sizeof(str));
    ASSERT_EQ(0, memcmp(str, buf, used));
  }
  // TAG_LINE

#else // WYRD_POSIX
  GTEST_SKIP() << "File access not implemented for non-POSIX systems.";
#endif // WYRD_POSIX
}


TEST(Persistence, skip_unknown_strategy)
{
#ifdef WYRD_POSIX
  char str[] = "The quick brown fox jumped over the lazy dog";

  // We read a previously written file with an unknown strategy; this needs to
  // be safely skipped.
  posix_context ctx{false, "test/unknown_strategy", WYRD_O_SKIP_UNKNOWN};

  // foo.bar
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "foo.bar",
        WYRD_PT_UINT16));
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "foo.bar",
        WYRD_MS_NAIVE_OVERRIDE));

  uint16_t val = 0;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_uint16(ctx.handle, "foo.bar",
        &val));
  ASSERT_EQ(1312, val); // XXX The last edit to set it to 42 is unreadable.

  // foo.baz - XXX is a unit16_t due to invalid edit
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "foo.baz",
        WYRD_PT_UINT16));
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "foo.baz",
        WYRD_MS_NAIVE_OVERRIDE));

  val = 0;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_uint16(ctx.handle, "foo.baz",
        &val));
  ASSERT_EQ(42, val);

  // quux
  char buf[500];
  size_t used = sizeof(buf);
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "quux",
        WYRD_PT_UTF8_STRING));
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "quux",
        WYRD_MS_NAIVE_OVERRIDE));

  buf[0] = '\0';
  used = sizeof(buf);
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_utf8string(ctx.handle, "quux",
        buf, &used));
  ASSERT_EQ(used, sizeof(str));
  ASSERT_EQ(0, memcmp(str, buf, used));

#else // WYRD_POSIX
  GTEST_SKIP() << "File access not implemented for non-POSIX systems.";
#endif // WYRD_POSIX
}



TEST(Persistence, multi_edit_multi_value_vessel)
{
  char str[] = "The quick brown fox jumped over the lazy dog";

  // Persistence means that a handle (via vessel_context) should contain the properties
  // written into them.
  // TAG_LINE
  {
    vessel_context ctx{false};

    // foo.bar
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 42, 1));

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_utf8string(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, str, sizeof(str), 1));

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 1312, 1));

    // foo.baz
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.baz",
          WYRD_MS_NAIVE_OVERRIDE, 42, 1));

    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_utf8string(ctx.handle, "foo.baz",
          WYRD_MS_NAIVE_OVERRIDE, str, sizeof(str), 1));

    // quux
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_utf8string(ctx.handle, "quux",
          WYRD_MS_NAIVE_OVERRIDE, str, sizeof(str), 1));

    // foo.bar again
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_uint16(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE, 42, 1));

  }
  // TAG_LINE
  {
    // FIXME vessel_context ctx{true}; // unlink after use
    vessel_context ctx{false};

    // foo.bar
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "foo.bar",
          WYRD_PT_UINT16));
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "foo.bar",
          WYRD_MS_NAIVE_OVERRIDE));

    uint16_t val = 0;
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_uint16(ctx.handle, "foo.bar",
          &val));
    ASSERT_EQ(42, val);

    // foo.baz
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "foo.baz",
          WYRD_PT_UTF8_STRING));
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "foo.baz",
          WYRD_MS_NAIVE_OVERRIDE));

    char buf[500];
    size_t used = sizeof(buf);
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_utf8string(ctx.handle, "foo.baz",
          buf, &used));
    ASSERT_EQ(used, sizeof(str));
    ASSERT_EQ(0, memcmp(str, buf, used));

    // quux
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_property_type(ctx.handle, "quux",
          WYRD_PT_UTF8_STRING));
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_check_merge_strategy(ctx.handle, "quux",
          WYRD_MS_NAIVE_OVERRIDE));

    buf[0] = '\0';
    used = sizeof(buf);
    ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_utf8string(ctx.handle, "quux",
          buf, &used));
    ASSERT_EQ(used, sizeof(str));
    ASSERT_EQ(0, memcmp(str, buf, used));
  }
  // TAG_LINE
}
