/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <wyrd/properties.h>

namespace {

struct context
{
  constexpr static char const * TEST_FILE = "testfile";

  struct wyrd_api *     api = nullptr;
  struct wyrd_handle *  handle = nullptr;

  inline context()
  {
    if (WYRD_ERR_SUCCESS != wyrd_create(&api)) {
      throw std::runtime_error{"Unable to create API"};
    }
    if (WYRD_ERR_SUCCESS != wyrd_open_file(api, &handle, TEST_FILE, WYRD_O_RW | WYRD_O_CREATE)) {
      throw std::runtime_error{"Unable to create handle"};
    }
  }

  inline ~context()
  {
    wyrd_close(&handle);
    wyrd_destroy(&api);
    ::unlink(TEST_FILE);
  }
};


static uint16_t observed_uint16 = 0;

void
test_callback_uint16(struct wyrd_handle * handle,
    char const * path, wyrd_property_type type,
    void const * value, size_t value_size, void * baton [[maybe_unused]])
{
  ASSERT_TRUE(handle && path && value && value_size);
  ASSERT_EQ(WYRD_PT_UINT16, type);

  observed_uint16 = *static_cast<uint16_t const *>(value);
}


void
test_callback_uint16_counting(struct wyrd_handle * handle,
    char const * path, wyrd_property_type type,
    void const * value, size_t value_size, void * baton [[maybe_unused]])
{
  ASSERT_TRUE(handle && path && value && value_size);
  ASSERT_EQ(WYRD_PT_UINT16, type);

  ASSERT_TRUE(baton);
  size_t * counter = static_cast<size_t *>(baton);
  (*counter)++;
}


} // anonymous namespace


TEST(Properties, set_single_property)
{
  context ctx;

  uint16_t value = 42;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_typed(ctx.handle,
        ".foo.bar", WYRD_PT_UINT16, WYRD_MS_NAIVE_OVERRIDE,
        &value, sizeof(value), 1));
}


TEST(Properties, set_get_property)
{
  context ctx;

  uint16_t value = 42;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_typed(ctx.handle,
        ".foo.bar", WYRD_PT_UINT16, WYRD_MS_NAIVE_OVERRIDE,
        &value, sizeof(value), 1));

  value = 0;
  size_t size = sizeof(value);
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property(ctx.handle, ".foo.bar",
        &value, &size));
  ASSERT_EQ(size, sizeof(value));
  ASSERT_EQ(42, value);
}


TEST(Properties, property_observer)
{
  context ctx;

  uint16_t value = 42;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_typed(ctx.handle,
        ".foo.bar", WYRD_PT_UINT16, WYRD_MS_NAIVE_OVERRIDE,
        &value, sizeof(value), 1));

  // Set observer
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_callback(ctx.handle,
        ".foo.bar", test_callback_uint16, nullptr));

  // Observe change
  observed_uint16 = 0;

  value = 123;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_typed(ctx.handle,
        ".foo.bar", WYRD_PT_UINT16, WYRD_MS_NAIVE_OVERRIDE,
        &value, sizeof(value), 1));

  ASSERT_EQ(observed_uint16, 123);

  // Clear observer
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_clear_property_callback(ctx.handle,
        ".foo.bar", test_callback_uint16, nullptr));

  // Don't observe change
  observed_uint16 = 0;

  value = 42;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_typed(ctx.handle,
        ".foo.bar", WYRD_PT_UINT16, WYRD_MS_NAIVE_OVERRIDE,
        &value, sizeof(value), 1));

  ASSERT_EQ(observed_uint16, 0);
}



TEST(Properties, multiple_observers)
{
  context ctx;

  // Set the same observer with two different batons
  size_t count1 = 0;
  size_t count2 = 0;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_callback(ctx.handle,
        ".foo.bar", test_callback_uint16_counting, &count1));
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_callback(ctx.handle,
        ".foo.bar", test_callback_uint16_counting, &count2));

  // Observe changes
  uint16_t value = 123;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_typed(ctx.handle,
        ".foo.bar", WYRD_PT_UINT16, WYRD_MS_NAIVE_OVERRIDE,
        &value, sizeof(value), 1));

  ASSERT_EQ(1, count1);
  ASSERT_EQ(1, count2);

  // Clear *one* of the observers
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_clear_property_callback(ctx.handle,
        ".foo.bar", test_callback_uint16_counting, &count2));

  // Observe change on the other callback
  value = 42;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_typed(ctx.handle,
        ".foo.bar", WYRD_PT_UINT16, WYRD_MS_NAIVE_OVERRIDE,
        &value, sizeof(value), 1));

  ASSERT_EQ(2, count1);
  ASSERT_EQ(1, count2);
}


TEST(Properties, same_observer_multiple_times)
{
  context ctx;

  // Set the same observer with the same baton multiple times
  size_t count = 0;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_add_property_callback(ctx.handle,
        ".foo.bar", test_callback_uint16_counting, &count));
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_add_property_callback(ctx.handle,
        ".foo.bar", test_callback_uint16_counting, &count));

  // Observe changes
  uint16_t value = 123;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_typed(ctx.handle,
        ".foo.bar", WYRD_PT_UINT16, WYRD_MS_NAIVE_OVERRIDE,
        &value, sizeof(value), 1));

  // Observer gets called *once*
  ASSERT_EQ(1, count);

  // Clear *one* of the observers
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_remove_property_callback(ctx.handle,
        ".foo.bar", test_callback_uint16_counting, &count));

  // Observe change still gets recorded by the other observer
  value = 42;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_typed(ctx.handle,
        ".foo.bar", WYRD_PT_UINT16, WYRD_MS_NAIVE_OVERRIDE,
        &value, sizeof(value), 1));

  ASSERT_EQ(2, count);

  // Remove other observer
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_remove_property_callback(ctx.handle,
        ".foo.bar", test_callback_uint16_counting, &count));

  // Observe change remains unobserved
  value = 321;
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_typed(ctx.handle,
        ".foo.bar", WYRD_PT_UINT16, WYRD_MS_NAIVE_OVERRIDE,
        &value, sizeof(value), 1));

  ASSERT_EQ(2, count);
}


namespace {

template <typename T>
struct accessors
{
};

#define TEST_SPECIALIZE_ACCESSOR(ctype, name) \
  template <> \
  struct accessors<ctype> \
  { \
    static constexpr auto setter = wyrd_set_property_ ## name; \
    static constexpr auto getter = wyrd_get_property_ ## name; \
  };

TEST_SPECIALIZE_ACCESSOR(int8_t, sint8);
TEST_SPECIALIZE_ACCESSOR(uint8_t, uint8);
TEST_SPECIALIZE_ACCESSOR(int16_t, sint16);
TEST_SPECIALIZE_ACCESSOR(uint16_t, uint16);
TEST_SPECIALIZE_ACCESSOR(int32_t, sint32);
TEST_SPECIALIZE_ACCESSOR(uint32_t, uint32);
TEST_SPECIALIZE_ACCESSOR(int64_t, sint64);
TEST_SPECIALIZE_ACCESSOR(uint64_t, uint64);

TEST_SPECIALIZE_ACCESSOR(float, float);
TEST_SPECIALIZE_ACCESSOR(double, double);


} // anonymous namespace


template <typename T>
class TypedProperties : public testing::Test
{
};

typedef testing::Types<
  int8_t,
  uint8_t,
  int16_t,
  uint16_t,
  int32_t,
  uint32_t,
  int64_t,
  uint64_t,
  float,
  double
> test_types;

TYPED_TEST_SUITE(TypedProperties, test_types);


TYPED_TEST(TypedProperties, construct_and_access)
{
  context ctx;

  // Set
  TypeParam value = 42;
  ASSERT_EQ(WYRD_ERR_SUCCESS, accessors<TypeParam>::setter(ctx.handle,
        ".foo.bar", WYRD_MS_NAIVE_OVERRIDE, value, 1));

  // Get
  value = 0;
  ASSERT_EQ(WYRD_ERR_SUCCESS, accessors<TypeParam>::getter(ctx.handle,
        "foo..bar", &value));
  ASSERT_EQ(42, value);
}


TEST(TypedProperties, construct_and_access_blob)
{
  // This test is exactly the same as the UTF-8 string test, except we can
  // put arbitrary bytes in.
  context ctx;

  // Set
  char value[] = "Hello,\0world!";
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_blob(ctx.handle,
        ".foo.bar", WYRD_MS_NAIVE_OVERRIDE, value, sizeof(value), 1));

  // Get
  for (size_t i = 0 ; i < sizeof(value) ; ++i) {
    value[i] = '\0';
  }
  size_t size = sizeof(value);
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_blob(ctx.handle,
        "foo..bar", value, &size));

  ASSERT_EQ(size, sizeof(value));
  ASSERT_EQ(0, memcmp(value, "Hello,\0world!", size));
}


TEST(TypedProperties, construct_and_access_utf8string)
{
  context ctx;

  // Set
  char value[] = "Hello, world!";
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_set_property_utf8string(ctx.handle,
        ".foo.bar", WYRD_MS_NAIVE_OVERRIDE, value, sizeof(value), 1));

  // Get
  for (size_t i = 0 ; i < sizeof(value) ; ++i) {
    value[i] = '\0';
  }
  size_t size = sizeof(value);
  ASSERT_EQ(WYRD_ERR_SUCCESS, wyrd_get_property_utf8string(ctx.handle,
        "foo..bar", value, &size));

  ASSERT_EQ(size, sizeof(value));
  ASSERT_EQ(0, strncmp(value, "Hello, world!", size));
}
