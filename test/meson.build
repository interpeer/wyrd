##############################################################################
# Tests

# Tests in a subproject are complicicated. You need all the compile and
# link flags from the enclosing project, etc.
if not meson.is_subproject() and get_option('build_extras')
  gtest = subproject('gtest')

  # See https://github.com/google/googletest/issues/813
  test_args = []
  if host_machine.system() == 'cygwin'
    test_args += ['-D_POSIX_C_SOURCE=200809L']
  endif

  # Google test issues this warning; disable it in *test* code only.
  if compiler_id == 'msvc'
    test_args = [
      '/wd4389',
    ]
  endif

  # We're building two tests:
  # - public_tests include *only* public headers
  # - private_tests include private headers

  fs = import('fs')
  data = [
    fs.copyfile('data' / 'unknown_strategy'),
  ]
  data_dep = declare_dependency(
    sources: data,
  )

  public_test_src = [
    'public' / 'basics.cpp',
    'public' / 'properties.cpp',
    'public' / 'persistence.cpp',

    'runner.cpp',
  ]

  private_test_src = [
    'private' / 'property_path.cpp',
    'private' / 'properties.cpp',
    'private' / 'serialization.cpp',
    'private' / 'merge_strategies.cpp',
    'private' / 'callbacks.cpp',

    'runner.cpp',
  ]
  if host_type == 'posix'
    private_test_src += [
      'private' / 'posix_handle.cpp',
    ]
  endif

  public_tests = executable('public_tests', public_test_src,
      dependencies: [
        main_build_dir, # XXX private headers include the build config
        dep_internal,
        gtest.get_variable('gtest_dep'),
        data_dep,
      ],
      cpp_args: test_args,
  )
  test('public_tests', public_tests)

  # Due to symbol visibility, private tests won't link for non-debug builds
  if bt in ['debug', 'debugoptimized']
    private_tests = executable('private_tests', private_test_src,
        include_directories: [libincludes],  # Also private headers
        dependencies: [
          main_build_dir, # XXX private headers include the build config
          dep_internal,
          gtest.get_variable('gtest_dep'),
          data_dep,
        ],
        cpp_args: cpp_define_args + test_args,
    )
    test('private_tests', private_tests)
  endif

  c_compiler = meson.get_compiler('c')
  c_compatibility = executable('c_compatibility', 'c_compatibility.c',
    dependencies: [
      dep_internal,
    ],
    link_language: 'c',
  )
  test('c_compatibility', c_compatibility)
endif
