/**
 * This file is part of wyrd.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <wyrd/api.h>

int main(int argc, char **argv)
{
  struct wyrd_api * api = NULL;
  wyrd_error_t err = WYRD_ERR_SUCCESS;

  err = wyrd_create(&api);
  assert(WYRD_ERR_SUCCESS == err);
  assert(NULL != api);

  err = wyrd_destroy(&api);
  assert(WYRD_ERR_SUCCESS == err);
  assert(NULL == api);

  exit(0);
}
